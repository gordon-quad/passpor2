/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _PASSCONF_H_
#define _PASSCONF_H_

#if !defined(PASS_PIN_LEN) || defined(__DOXYGEN__)
#define PASS_PIN_RETRIES       5
#endif

#if !defined(PASS_PIN_LEN) || defined(__DOXYGEN__)
#define PASS_PIN_LEN           4
#endif

#if !defined(PASS_PIN_DATA) || defined(__DOXYGEN__)
#define PASS_PIN_DATA          {0, 0, 0, 0}
#endif

#if !defined(PASS_PUC_LEN) || defined(__DOXYGEN__)
#define PASS_PUC_LEN           20
#endif

#if !defined(PASS_PUC_DATA) || defined(__DOXYGEN__)
#define PASS_PUC_DATA          {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
#endif

#if !defined(PASS_MASTERKEY_DATA) || defined(__DOXYGEN__)
#define PASS_MASTERKEY_DATA    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
#endif

#endif//_PASSCONF_H_
