/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _SHELLCONF_H_
#define _SHELLCONF_H_

/**
 * @brief   Shell maximum input line length.
 */
#if !defined(SHELL_MAX_LINE_LENGTH) || defined(__DOXYGEN__)
#define SHELL_MAX_LINE_LENGTH       256
#endif

/**
 * @brief   Shell maximum arguments per command.
 */
#if !defined(SHELL_MAX_ARGUMENTS) || defined(__DOXYGEN__)
#define SHELL_MAX_ARGUMENTS         4
#endif

/**
 * @brief   Shell maximum command history.
 */
#if !defined(SHELL_MAX_HIST_BUFF) || defined(__DOXYGEN__)
#define SHELL_MAX_HIST_BUFF         8 * SHELL_MAX_LINE_LENGTH
#endif

/**
 * @brief   Enable shell command history
 */
#if !defined(SHELL_USE_HISTORY) || defined(__DOXYGEN__)
#define SHELL_USE_HISTORY           FALSE
#endif

/**
 * @brief   Enable shell command completion
 */
#if !defined(SHELL_USE_COMPLETION) || defined(__DOXYGEN__)
#define SHELL_USE_COMPLETION        FALSE
#endif

/**
 * @brief   Shell Maximum Completions (Set to max commands with common prefix)
 */
#if !defined(SHELL_MAX_COMPLETIONS) || defined(__DOXYGEN__)
#define SHELL_MAX_COMPLETIONS       8
#endif

/**
 * @brief   Enable shell escape sequence processing
 */
#if !defined(SHELL_USE_ESC_SEQ) || defined(__DOXYGEN__)
#define SHELL_USE_ESC_SEQ           FALSE
#endif

/**
 * @brief   Prompt string
 */
#if !defined(SHELL_PROMPT_STR) || defined(__DOXYGEN__)
#define SHELL_PROMPT_STR            "ch> "
#endif

/**
 * @brief   Newline string
 */
#if !defined(SHELL_NEWLINE_STR) || defined(__DOXYGEN__)
#define SHELL_NEWLINE_STR            "\r\n"
#endif

#if !defined(SHELL_CMD_EXIT_ENABLED) || defined(__DOXYGEN__)
#define SHELL_CMD_EXIT_ENABLED              TRUE
#endif

#if !defined(SHELL_CMD_INFO_ENABLED) || defined(__DOXYGEN__)
#define SHELL_CMD_INFO_ENABLED              TRUE
#endif

#if !defined(SHELL_CMD_ECHO_ENABLED) || defined(__DOXYGEN__)
#define SHELL_CMD_ECHO_ENABLED              TRUE
#endif

#if !defined(SHELL_CMD_SYSTIME_ENABLED) || defined(__DOXYGEN__)
#define SHELL_CMD_SYSTIME_ENABLED           TRUE
#endif

#if !defined(SHELL_CMD_MEM_ENABLED) || defined(__DOXYGEN__)
#define SHELL_CMD_MEM_ENABLED               FALSE
#endif

#if !defined(SHELL_CMD_THREADS_ENABLED) || defined(__DOXYGEN__)
#define SHELL_CMD_THREADS_ENABLED           TRUE
#endif

#if !defined(SHELL_CMD_TEST_ENABLED) || defined(__DOXYGEN__)
#define SHELL_CMD_TEST_ENABLED              FALSE
#endif

#if !defined(SHELL_CMD_TEST_WA_SIZE) || defined(__DOXYGEN__)
#define SHELL_CMD_TEST_WA_SIZE              THD_WORKING_AREA_SIZE(256)
#endif


#endif//_SHELLCONF_H_
