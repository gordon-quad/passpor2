EESchema Schematic File Version 4
LIBS:passpor2-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C7
U 1 1 5D84397F
P 2150 1600
F 0 "C7" H 2265 1646 50  0000 L CNN
F 1 "100n" H 2265 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2188 1450 50  0001 C CNN
F 3 "~" H 2150 1600 50  0001 C CNN
	1    2150 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5D95510C
P 3950 3300
F 0 "D1" V 3989 3182 50  0000 R CNN
F 1 "LED_RED" V 3898 3182 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 3950 3300 50  0001 C CNN
F 3 "~" H 3950 3300 50  0001 C CNN
	1    3950 3300
	0    -1   -1   0   
$EndComp
$Comp
L Regulator_Linear:MCP1700-3302E_SOT23 U2
U 1 1 5D957B83
P 4650 1600
F 0 "U2" H 4650 1842 50  0000 C CNN
F 1 "MCP1700-3302E_SOT23" H 4650 1751 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4650 1825 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001826D.pdf" H 4650 1600 50  0001 C CNN
	1    4650 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5D95CEE5
P 4100 1850
F 0 "C9" H 4215 1896 50  0000 L CNN
F 1 "100n" H 4215 1805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4138 1700 50  0001 C CNN
F 3 "~" H 4100 1850 50  0001 C CNN
	1    4100 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5D95D7F3
P 5150 1850
F 0 "C10" H 5265 1896 50  0000 L CNN
F 1 "4.7u" H 5265 1805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5188 1700 50  0001 C CNN
F 3 "~" H 5150 1850 50  0001 C CNN
	1    5150 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5D95DCFE
P 1750 1600
F 0 "C6" H 1865 1646 50  0000 L CNN
F 1 "100n" H 1865 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1788 1450 50  0001 C CNN
F 3 "~" H 1750 1600 50  0001 C CNN
	1    1750 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5D95E393
P 1350 1600
F 0 "C5" H 1465 1646 50  0000 L CNN
F 1 "100n" H 1465 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1388 1450 50  0001 C CNN
F 3 "~" H 1350 1600 50  0001 C CNN
	1    1350 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5D961951
P 3950 3000
F 0 "R3" H 4020 3046 50  0000 L CNN
F 1 "220" H 4020 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3880 3000 50  0001 C CNN
F 3 "~" H 3950 3000 50  0001 C CNN
	1    3950 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5D965D34
P 4500 3000
F 0 "R4" H 4570 3046 50  0000 L CNN
F 1 "220" H 4570 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4430 3000 50  0001 C CNN
F 3 "~" H 4500 3000 50  0001 C CNN
	1    4500 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5D966055
P 950 1600
F 0 "C2" H 1065 1646 50  0000 L CNN
F 1 "100n" H 1065 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 988 1450 50  0001 C CNN
F 3 "~" H 950 1600 50  0001 C CNN
	1    950  1600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5D966916
P 1000 3100
F 0 "R1" H 1070 3146 50  0000 L CNN
F 1 "100k" H 1070 3055 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 930 3100 50  0001 C CNN
F 3 "~" H 1000 3100 50  0001 C CNN
	1    1000 3100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5D96D94C
P 1200 4050
F 0 "#PWR03" H 1200 3800 50  0001 C CNN
F 1 "GND" H 1205 3877 50  0000 C CNN
F 2 "" H 1200 4050 50  0001 C CNN
F 3 "" H 1200 4050 50  0001 C CNN
	1    1200 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5D972761
P 4650 1900
F 0 "#PWR019" H 4650 1650 50  0001 C CNN
F 1 "GND" H 4655 1727 50  0000 C CNN
F 2 "" H 4650 1900 50  0001 C CNN
F 3 "" H 4650 1900 50  0001 C CNN
	1    4650 1900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR020
U 1 1 5D972B74
P 5150 1400
F 0 "#PWR020" H 5150 1250 50  0001 C CNN
F 1 "VCC" H 5167 1573 50  0000 C CNN
F 2 "" H 5150 1400 50  0001 C CNN
F 3 "" H 5150 1400 50  0001 C CNN
	1    5150 1400
	1    0    0    -1  
$EndComp
$Comp
L power:VBUS #PWR014
U 1 1 5D972F35
P 4100 1400
F 0 "#PWR014" H 4100 1250 50  0001 C CNN
F 1 "VBUS" H 4115 1573 50  0000 C CNN
F 2 "" H 4100 1400 50  0001 C CNN
F 3 "" H 4100 1400 50  0001 C CNN
	1    4100 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5D987172
P 900 3900
F 0 "#PWR02" H 900 3650 50  0001 C CNN
F 1 "GND" H 905 3727 50  0000 C CNN
F 2 "" H 900 3900 50  0001 C CNN
F 3 "" H 900 3900 50  0001 C CNN
	1    900  3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3850 1750 3850
Wire Wire Line
	1900 3950 1750 3950
$Comp
L Device:LED D2
U 1 1 5D98E3D9
P 4500 3300
F 0 "D2" V 4539 3182 50  0000 R CNN
F 1 "LED_GREEN" V 4448 3182 50  0000 R CNN
F 2 "LED_SMD:LED_0402_1005Metric" H 4500 3300 50  0001 C CNN
F 3 "~" H 4500 3300 50  0001 C CNN
	1    4500 3300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5D99C46F
P 2150 2050
F 0 "#PWR07" H 2150 1800 50  0001 C CNN
F 1 "GND" H 2155 1877 50  0000 C CNN
F 2 "" H 2150 2050 50  0001 C CNN
F 3 "" H 2150 2050 50  0001 C CNN
	1    2150 2050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR06
U 1 1 5D99C83B
P 2150 1150
F 0 "#PWR06" H 2150 1000 50  0001 C CNN
F 1 "VCC" H 2167 1323 50  0000 C CNN
F 2 "" H 2150 1150 50  0001 C CNN
F 3 "" H 2150 1150 50  0001 C CNN
	1    2150 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  1450 950  1300
Wire Wire Line
	950  1300 1350 1300
Wire Wire Line
	2150 1300 2150 1150
Connection ~ 2150 1300
Wire Wire Line
	2150 1900 2150 2050
Wire Wire Line
	2150 1750 2150 1900
Connection ~ 2150 1900
Wire Wire Line
	2150 1900 1750 1900
Wire Wire Line
	1750 1900 1750 1750
Wire Wire Line
	1750 1900 1350 1900
Wire Wire Line
	1350 1900 1350 1750
Connection ~ 1750 1900
Wire Wire Line
	1350 1900 950  1900
Wire Wire Line
	950  1900 950  1750
Connection ~ 1350 1900
Wire Wire Line
	1350 1450 1350 1300
Connection ~ 1350 1300
Wire Wire Line
	1350 1300 1750 1300
Wire Wire Line
	1750 1450 1750 1300
Connection ~ 1750 1300
Wire Wire Line
	1750 1300 2150 1300
Wire Wire Line
	2150 1450 2150 1300
Wire Wire Line
	4100 1400 4100 1600
Wire Wire Line
	4100 1600 4350 1600
Connection ~ 4100 1600
Wire Wire Line
	4100 1600 4100 1700
Wire Wire Line
	5150 1700 5150 1600
Wire Wire Line
	5150 1600 4950 1600
Wire Wire Line
	5150 1600 5150 1400
Connection ~ 5150 1600
$Comp
L power:GND #PWR015
U 1 1 5D9B20CC
P 4100 2000
F 0 "#PWR015" H 4100 1750 50  0001 C CNN
F 1 "GND" H 4105 1827 50  0000 C CNN
F 2 "" H 4100 2000 50  0001 C CNN
F 3 "" H 4100 2000 50  0001 C CNN
	1    4100 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5D9B231B
P 5150 2000
F 0 "#PWR021" H 5150 1750 50  0001 C CNN
F 1 "GND" H 5155 1827 50  0000 C CNN
F 2 "" H 5150 2000 50  0001 C CNN
F 3 "" H 5150 2000 50  0001 C CNN
	1    5150 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5D9EBB46
P 3950 3450
F 0 "#PWR013" H 3950 3200 50  0001 C CNN
F 1 "GND" H 3955 3277 50  0000 C CNN
F 2 "" H 3950 3450 50  0001 C CNN
F 3 "" H 3950 3450 50  0001 C CNN
	1    3950 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5D9EC2E6
P 4500 3450
F 0 "#PWR017" H 4500 3200 50  0001 C CNN
F 1 "GND" H 4505 3277 50  0000 C CNN
F 2 "" H 4500 3450 50  0001 C CNN
F 3 "" H 4500 3450 50  0001 C CNN
	1    4500 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2850 3950 2700
Wire Wire Line
	3950 2700 3800 2700
Wire Wire Line
	4500 2850 4500 2700
Wire Wire Line
	4500 2700 4350 2700
Text Label 3800 2700 0    50   ~ 0
LEDR
Text Label 4350 2700 0    50   ~ 0
LEDG
Wire Wire Line
	4600 4200 4450 4200
Text Label 3900 4200 0    50   ~ 0
BTN
Wire Wire Line
	3900 4200 4050 4200
$Comp
L power:VCC #PWR08
U 1 1 5DA01922
P 2550 2650
F 0 "#PWR08" H 2550 2500 50  0001 C CNN
F 1 "VCC" H 2567 2823 50  0000 C CNN
F 2 "" H 2550 2650 50  0001 C CNN
F 3 "" H 2550 2650 50  0001 C CNN
	1    2550 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2950 2700 2700
Wire Wire Line
	2700 2700 2600 2700
Wire Wire Line
	2550 2700 2550 2650
Wire Wire Line
	2600 2950 2600 2700
Connection ~ 2600 2700
Wire Wire Line
	2600 2700 2550 2700
Wire Wire Line
	2500 2950 2500 2700
Wire Wire Line
	2500 2700 2550 2700
Connection ~ 2550 2700
Wire Wire Line
	2400 2950 2400 2700
Wire Wire Line
	2400 2700 2500 2700
Connection ~ 2500 2700
$Comp
L power:VCC #PWR05
U 1 1 5DA06686
P 1650 2500
F 0 "#PWR05" H 1650 2350 50  0001 C CNN
F 1 "VCC" H 1667 2673 50  0000 C CNN
F 2 "" H 1650 2500 50  0001 C CNN
F 3 "" H 1650 2500 50  0001 C CNN
	1    1650 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5D95D29F
P 900 3750
F 0 "C1" H 1015 3796 50  0000 L CNN
F 1 "4.7u" H 1015 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 938 3600 50  0001 C CNN
F 3 "~" H 900 3750 50  0001 C CNN
	1    900  3750
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR04
U 1 1 5DA20503
P 1250 3400
F 0 "#PWR04" H 1250 3250 50  0001 C CNN
F 1 "VCC" H 1250 3550 50  0000 C CNN
F 2 "" H 1250 3400 50  0001 C CNN
F 3 "" H 1250 3400 50  0001 C CNN
	1    1250 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 3400 1250 3650
Wire Wire Line
	1250 3650 1900 3650
$Comp
L power:GND #PWR09
U 1 1 5DA22C51
P 2600 6350
F 0 "#PWR09" H 2600 6100 50  0001 C CNN
F 1 "GND" H 2605 6177 50  0000 C CNN
F 2 "" H 2600 6350 50  0001 C CNN
F 3 "" H 2600 6350 50  0001 C CNN
	1    2600 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 6150 2800 6250
Wire Wire Line
	2800 6250 2700 6250
Wire Wire Line
	2600 6250 2600 6350
Wire Wire Line
	2700 6150 2700 6250
Connection ~ 2700 6250
Wire Wire Line
	2700 6250 2600 6250
Wire Wire Line
	2600 6150 2600 6250
Connection ~ 2600 6250
Wire Wire Line
	2500 6150 2500 6250
Wire Wire Line
	2500 6250 2600 6250
Wire Wire Line
	2400 6150 2400 6250
Wire Wire Line
	2400 6250 2500 6250
Connection ~ 2500 6250
$Comp
L Device:R R2
U 1 1 5D954D45
P 1650 2850
F 0 "R2" H 1720 2896 50  0000 L CNN
F 1 "100k" H 1720 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1580 2850 50  0001 C CNN
F 3 "~" H 1650 2850 50  0001 C CNN
	1    1650 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 3350 1900 3350
Wire Wire Line
	1350 3000 1350 3100
Wire Wire Line
	1650 3000 1650 3150
Wire Wire Line
	1650 3150 1900 3150
Text Label 1650 3150 0    50   ~ 0
NRST
Text Label 1350 3350 0    50   ~ 0
BOOT0
Text Label 1750 5750 0    50   ~ 0
BTN
Wire Wire Line
	1900 5750 1750 5750
Text Label 1700 5950 0    50   ~ 0
LEDR
Wire Wire Line
	1700 5950 1900 5950
Wire Wire Line
	1900 5850 1700 5850
Text Label 1700 5850 0    50   ~ 0
LEDG
$Comp
L power:VBUS #PWR010
U 1 1 5DA53C49
P 3350 5200
F 0 "#PWR010" H 3350 5050 50  0001 C CNN
F 1 "VBUS" H 3365 5373 50  0000 C CNN
F 2 "" H 3350 5200 50  0001 C CNN
F 3 "" H 3350 5200 50  0001 C CNN
	1    3350 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 5650 3500 5650
Text Label 3300 5650 0    50   ~ 0
D+
Wire Wire Line
	3200 5550 3500 5550
Text Label 3300 5550 0    50   ~ 0
D-
Text Label 3800 5650 0    50   ~ 0
D-
Text Label 3800 5550 0    50   ~ 0
D+
Wire Wire Line
	3800 5650 3950 5650
Wire Wire Line
	3800 5550 3950 5550
Wire Wire Line
	3350 5200 3350 5350
Wire Wire Line
	3350 5350 3200 5350
$Comp
L power:VBUS #PWR012
U 1 1 5DA6604E
P 3800 5250
F 0 "#PWR012" H 3800 5100 50  0001 C CNN
F 1 "VBUS" H 3815 5423 50  0000 C CNN
F 2 "" H 3800 5250 50  0001 C CNN
F 3 "" H 3800 5250 50  0001 C CNN
	1    3800 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5DA6BA13
P 4250 5950
F 0 "#PWR016" H 4250 5700 50  0001 C CNN
F 1 "GND" H 4255 5777 50  0000 C CNN
F 2 "" H 4250 5950 50  0001 C CNN
F 3 "" H 4250 5950 50  0001 C CNN
	1    4250 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5250 3800 5350
Wire Wire Line
	3800 5350 3950 5350
$Comp
L power:GND #PWR01
U 1 1 5DAA565D
P 700 3250
F 0 "#PWR01" H 700 3000 50  0001 C CNN
F 1 "GND" H 705 3077 50  0000 C CNN
F 2 "" H 700 3250 50  0001 C CNN
F 3 "" H 700 3250 50  0001 C CNN
	1    700  3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 3100 1350 3100
Connection ~ 1350 3100
Wire Wire Line
	1350 3100 1350 3350
NoConn ~ 3200 4450
NoConn ~ 3200 4550
NoConn ~ 3200 4650
NoConn ~ 3200 4750
NoConn ~ 3200 4850
NoConn ~ 3200 4950
NoConn ~ 3200 5050
NoConn ~ 3200 5150
NoConn ~ 3200 5250
NoConn ~ 3200 5450
NoConn ~ 1900 4250
NoConn ~ 1900 4350
NoConn ~ 1900 4550
NoConn ~ 1900 4650
NoConn ~ 1900 4850
NoConn ~ 1900 4950
NoConn ~ 1900 5050
NoConn ~ 1900 5150
NoConn ~ 1900 5250
NoConn ~ 1900 5350
NoConn ~ 1900 5450
NoConn ~ 1900 5550
NoConn ~ 3200 5750
NoConn ~ 3200 5850
NoConn ~ 3200 5950
Wire Wire Line
	900  3550 900  3600
Wire Wire Line
	900  3550 1900 3550
$Comp
L power:GND #PWR011
U 1 1 5DB42A6D
P 3400 6350
F 0 "#PWR011" H 3400 6100 50  0001 C CNN
F 1 "GND" H 3405 6177 50  0000 C CNN
F 2 "" H 3400 6350 50  0001 C CNN
F 3 "" H 3400 6350 50  0001 C CNN
	1    3400 6350
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5DB42E99
P 3400 6250
F 0 "#FLG01" H 3400 6325 50  0001 C CNN
F 1 "PWR_FLAG" H 3400 6423 50  0000 C CNN
F 2 "" H 3400 6250 50  0001 C CNN
F 3 "~" H 3400 6250 50  0001 C CNN
	1    3400 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 6250 3400 6350
$Comp
L MCU_ST_STM32F4:STM32F401CBUx U1
U 1 1 5D84003E
P 2600 4550
F 0 "U1" H 2550 6331 50  0000 C CNN
F 1 "STM32F401CBUx" H 2550 6240 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-48-1EP_7x7mm_P0.5mm_EP5.6x5.6mm" H 2000 3050 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00086815.pdf" H 2600 4550 50  0001 C CNN
	1    2600 4550
	1    0    0    -1  
$EndComp
NoConn ~ 1900 5650
NoConn ~ 1900 4750
NoConn ~ 1900 4150
Wire Wire Line
	850  3100 700  3100
Wire Wire Line
	700  3100 700  3250
$Comp
L power:GND #PWR0101
U 1 1 5D9F0EB0
P 4600 4300
F 0 "#PWR0101" H 4600 4050 50  0001 C CNN
F 1 "GND" H 4605 4127 50  0000 C CNN
F 2 "" H 4600 4300 50  0001 C CNN
F 3 "" H 4600 4300 50  0001 C CNN
	1    4600 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4200 4600 4300
$Comp
L usb_a:USB_A J1
U 1 1 5D9B3AC6
P 4250 5550
F 0 "J1" H 4020 5539 50  0000 R CNN
F 1 "USB_A" H 4020 5448 50  0000 R CNN
F 2 "passpor2_board:CONN-USB-A-PCB-TRACES" H 4400 5500 50  0001 C CNN
F 3 " ~" H 4400 5500 50  0001 C CNN
	1    4250 5550
	-1   0    0    -1  
$EndComp
$Comp
L Device:Crystal_GND2_Small Y1
U 1 1 5D9F9266
P 1350 3900
F 0 "Y1" V 1304 3988 50  0000 L CNN
F 1 "12MHz" V 1395 3988 50  0000 L CNN
F 2 "Crystal:Resonator_SMD_muRata_CSTxExxV-3Pin_3.0x1.1mm" H 1350 3900 50  0001 C CNN
F 3 "~" H 1350 3900 50  0001 C CNN
	1    1350 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 3850 1750 3750
Wire Wire Line
	1750 3750 1350 3750
Wire Wire Line
	1350 3750 1350 3800
Wire Wire Line
	1750 3950 1750 4050
Wire Wire Line
	1750 4050 1350 4050
Wire Wire Line
	1350 4050 1350 4000
Wire Wire Line
	1200 4050 1200 3900
Wire Wire Line
	1200 3900 1250 3900
$Comp
L Switch:SW_Push SW1
U 1 1 5DA12E22
P 4250 4200
F 0 "SW1" H 4250 4485 50  0000 C CNN
F 1 "SW_Push" H 4250 4394 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_EVQQ2" H 4250 4400 50  0001 C CNN
F 3 "~" H 4250 4400 50  0001 C CNN
	1    4250 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint BP1
U 1 1 5DB78E43
P 1350 3000
F 0 "BP1" H 1408 3118 50  0000 L CNN
F 1 "TestPoint" H 1408 3027 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 1550 3000 50  0001 C CNN
F 3 "~" H 1550 3000 50  0001 C CNN
	1    1350 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 2500 1650 2650
$Comp
L Connector:TestPoint VP1
U 1 1 5DB97A4C
P 1800 2550
F 0 "VP1" H 1858 2668 50  0000 L CNN
F 1 "TestPoint" H 1858 2577 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2000 2550 50  0001 C CNN
F 3 "~" H 2000 2550 50  0001 C CNN
	1    1800 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 2550 1800 2650
Wire Wire Line
	1800 2650 1650 2650
Connection ~ 1650 2650
Wire Wire Line
	1650 2650 1650 2700
$EndSCHEMATC
