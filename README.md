# Passpor2

Firmware for Espruino Pico or custom hardware board Passpor3 that implements hardware [SuperGenPass](https://chriszarate.github.io/supergenpass/)
alternative using offloaded master password and USB HID to type generated passwords back.

## Disclaimer

The device does not include any form of smartcard-like technology and generally purely protected from
invasive attacks. STM32F401 include some sort of read-out protection however long history of miserable
failures of chip manufacturers that are not specialized on physical security to make any form of on-chip 
data protection dictates that you should not rely on that. Device has some simple PIN protection with
limited amount of retries but loss and/or stealing of the device must be treated as if master key is
compromised.

There is no firmware verification algorithms implemented in embedded DFU bootloader that is used to flash
the device, therefore it is user's responsibility to make sure trusted firmware blob is built and uploaded
to the device.

## Building

```
git submodule update --init --recursive
make
```

## Flashing

Flashing can be done using `dfu-util`:

```
dfu-util -a 0 -d 0483:df11 -D build/passpor2.bin -s 0x08000000
```

## PIN authentication

Device is going to use PIN-code and PUC specified in passconf.h (which could be initialized randomly
during the build or could be provided prior to build).

PIN or PUC is entered using sequence sort presses of device push-button.  Every digit of PIN/PUC
is encoded with sequence of short-button presses. Each digit is acknowledged by device with short
red led blink, long press finishes PIN-input. After entering PIN device will flash green led multiple
times if PIN has been entered correctly and present USB interface to PC. If PIN was entered incorrectly
it will flash red led multiple times. If number of PIN retries is exceeded device will blink red led
when inserted and only PUC is accepted.

## Basic usage

After authentication is complete device present itself as USB keyboard and serial port CDC. CDC is 
used to feed device with data that used during password generation.

Every password is armed before typing by solid red led. When device is armed long press of push-button
will type armed password using USB keyboard device.

If device is not armed - short press of push-button will arm first static password and every
consequitive press will cycle through static passwords.

CDC commands can be used to arm different passwords as well:

- `static n` - arm nth static password (equal of short-pressing push-button n+1 time)
- `otp80_key n` - arm nth base32-encoded 80-bit secret key for OTP generation (PUC required)
- `otp80_code n` - arm TOTP code using nth 80-bit secret key
- `otp160_key n` - arm nth base32-encoded 160-bit secret key for OTP generation (PUC required)
- `otp160_code n` - arm TOTP code using nth 160-bit secret key
- `genpass n url` - arm nth password derived for URL

Auxiliary commands that do not arm the device:

- `set_rtc YYYYMMDDhhmmss` - set RTC time that is used for TOTP code generation
- `dfu` - enter dfu bootloader for firmware upgrade (PUC required)
- `puc` - enter PUC for privileged commands

## Password derivation

Algorithm used for key/password derivation is following:

```
OTP_KEY = HMAC("otp:hex($n)>", masterkey)[0:OTP_KEY_SIZE]
URL_KEY = HMAC("url:$url:hex($n)", masterkey)
STATIC_KEY = HMAC("static:hex($n)", masterkey)

PASSWORD_STRING = dict[$i] + "." for each $i in bit_split($KEY, 10) + "123!@#"
OTP_KEY_STRING = base32($OTP_KEY)
```

dict is a dictionary of 1024 4-letter words to make a potentially memorizable password,
and "123!@#" string is added to each password to satisfy most of stupid online password
rules.

