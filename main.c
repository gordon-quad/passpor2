/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"

#include "chprintf.h"
#include "shell.h"

#include "usbcfg.h"
#include "usbhid.h"

#include "led.h"
#include "typer.h"
#include "btn.h"
#include "ctrl.h"
#include "auth.h"

#include "btldr.h"

#include "user_cmd.h"

/*===========================================================================*/
/* Initialization and main thread.                                           */
/*===========================================================================*/

static THD_WORKING_AREA(shell_wa, SHELL_WA_SIZE);
/*
 * Application entry point.
 */
int main(void)
{
    /*
     * Check if we need to jump to bootloader
     */
    btldr_check_bootloader();

    /*
     * System initializations.
     * - HAL initialization, this also initializes the configured device drivers
     *   and performs the board-specific initializations.
     * - Kernel initialization, the main() function becomes a thread and the
     *   RTOS is active.
     */
    halInit();
    chSysInit();

    /*
     * Shell manager initialization.
     */
    shellInit();

    /*
     * LED controller initialization
     */
    led_init();

    /*
     * Typer initialization
     */
    typer_init();

    /*
     * Button controller initialization
     */
    btn_init();

    /*
     * Main controller initialization
     */
    ctrl_init();

    /*
     * Authenitcator initialization
     */
    auth_init();

    /*
     * Initializes a serial-over-USB CDC driver.
     */
    sduObjectInit(&SDU1);
    sduStart(&SDU1, &serusbcfg);

    /*
     * Initializes USB HID driver.
     */
    usbhidObjectInit(&USBHID1);
    usbhidStart(&USBHID1, &usbhidcfg);

    /*
     * Start LED controller threads
     */
    led_start();

    /*
     * Button thread
     */
    btn_start();

    /*
     * Authenticate user
     */
    auth_start();
    auth_wait();

    /*
     * Main controller thread
     */
    ctrl_start();

    /*
     * Typer thread
     */
    typer_start();

    /*
     * Activates the USB driver and then the USB bus pull-up on D+.
     * Note, a delay is inserted in order to not have to disconnect the cable
     * after a reset.
     */
    usbDisconnectBus(serusbcfg.usbp);
    chThdSleepMilliseconds(1000);
    usbStart(serusbcfg.usbp, &usbcfg);
    usbConnectBus(serusbcfg.usbp);

    /*
     * Normal main() thread activity, spawning shells.
     */
    while (true)
    {
        if (SDU1.config->usbp->state == USB_ACTIVE)
        {
            thread_t *shelltp = chThdCreateStatic(shell_wa, sizeof(shell_wa),
                    NORMALPRIO + 1, shellThread, (void *)&shell_cfg);
            chThdWait(shelltp);               /* Waiting termination.             */
        }
        chThdSleepMilliseconds(1000);
    }
}
