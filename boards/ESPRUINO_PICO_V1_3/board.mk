# List of all the board related files.
BOARDSRC = boards/ESPRUINO_PICO_V1_3/board.c

# Required include directories
BOARDINC = boards/ESPRUINO_PICO_V1_3

# Shared variables
ALLCSRC += $(BOARDSRC)
ALLINC  += $(BOARDINC)
