# List of all the board related files.
BOARDSRC = boards/PASSPOR3/board.c

# Required include directories
BOARDINC = boards/PASSPOR3

# Shared variables
ALLCSRC += $(BOARDSRC)
ALLINC  += $(BOARDINC)
