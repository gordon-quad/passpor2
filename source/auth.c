/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <stdio.h>

#include "hal.h"
#include "btn.h"
#include "led.h"
#include "auth.h"
#include "jrnl.h"

#include "passconf.h"

#define EVENT_SHORT_PRESS 0
#define EVENT_LONG_PRESS  1

#define NUM_TO_BITMASK(n) (0xff & (0xff >> (8 - n)))

static THD_WORKING_AREA(auth_wa, 128);

static thread_t *auth_pid;

static event_listener_t btn0_lst, btn1_lst;

static uint8_t pin[PASS_PIN_LEN] = PASS_PIN_DATA;
static uint8_t puc[PASS_PUC_LEN] = PASS_PUC_DATA;

static bool unlocked = false;

static uint8_t sub_bit(uint8_t v)
{
    for (uint8_t i = 0x80; i > 0; i >>= 1)
        if (v & i)
            return v & (~i);
    return 0;
}

THD_FUNCTION(auth_thread, p)
{
    (void)p;
    uint8_t c = 0, i = 0;
    bool pin_correct = true;
    bool puc_correct = true;
    sysinterval_t timeout = TIME_INFINITE;

    chRegSetThreadName("auth");
    chEvtRegister(&btn_short_press_evt, &btn0_lst, EVENT_SHORT_PRESS);
    chEvtRegister(&btn_long_press_evt, &btn1_lst, EVENT_LONG_PRESS);

    unlocked = false;

    if (journal_read_value() == JOURNAL_EMPTY)
        journal_write_value(NUM_TO_BITMASK(PASS_PIN_RETRIES));

    pin_correct = journal_read_value() != 0;
    if (!pin_correct)
        led_set(LED_RED, LED_MODE_LONG_FLASH);

    while (1)
    {
        eventmask_t evts;
        evts = chEvtWaitOneTimeout(ALL_EVENTS, timeout);

        if (!evts)
        {
            if (i < PASS_PIN_LEN)
                pin_correct = pin_correct && (pin[i] == c);
            else
                pin_correct = false;

            if (i < PASS_PUC_LEN)
                puc_correct = puc_correct && (puc[i] == c);
            else
                puc_correct = false;

            ++i;
            c = 0;
            timeout = TIME_INFINITE;
            led_set(LED_RED, LED_MODE_SINGLE_SHORT_FLASH);
        }
        if (evts & EVENT_MASK(EVENT_SHORT_PRESS))
        {
            ++c;
            timeout = CLICK_SERIES_TIMEOUT;
        }
        if (evts & EVENT_MASK(EVENT_LONG_PRESS))
        {
            c = 0;
            pin_correct = pin_correct && (i == PASS_PIN_LEN);
            puc_correct = puc_correct && (i == PASS_PUC_LEN);
            if (pin_correct)
            {
                journal_write_value(NUM_TO_BITMASK(PASS_PIN_RETRIES));
                led_set(LED_GREEN, LED_MODE_LONG_FLASH);
                break;
            }
            else if (puc_correct)
            {
                journal_write_value(NUM_TO_BITMASK(PASS_PIN_RETRIES));
                led_set(LED_GREEN, LED_MODE_LONG_FLASH);
                led_set(LED_RED, LED_MODE_LONG_FLASH);
                unlocked = true;
                break;
            }
            else
            {
                journal_write_value(sub_bit(journal_read_value()));
                led_set(LED_RED, LED_MODE_LONG_FLASH);
                pin_correct = journal_read_value() != 0;
                puc_correct = true;
                timeout = TIME_INFINITE;
                i = 0;
            }
        }
    }
}

void auth_init(void)
{
    // Nothing to be done
}

void auth_start(void)
{
    auth_pid = chThdCreateStatic(auth_wa, sizeof(auth_wa),
                       NORMALPRIO, auth_thread, NULL);
}

void auth_wait(void)
{
    chThdWait(auth_pid);
}

bool auth_is_admin_unlocked(void)
{
    return unlocked;
}

void auth_admin_unlock(char *code)
{
    bool correct = strlen(code) == PASS_PUC_LEN;
    uint8_t i;
    for (i = 0; (i < PASS_PUC_LEN) && *code; ++code, ++i)
        correct = correct && ((*code - '0') == puc[i]);
    unlocked = correct;
}
