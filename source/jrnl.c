/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "jrnl.h"

static uint8_t journal[JOURNAL_SIZE] __attribute__((__section__(".user"))) = { [0 ... JOURNAL_SIZE-1] = 0xff };
static int i = -1;

#define FLASH_KEY1 0x45670123
#define FLASH_KEY2 0xCDEF89AB

void flash_unlock(void)
{
    chSysLock();
    while (FLASH->SR & FLASH_SR_BSY);
    FLASH->KEYR = FLASH_KEY1;
    FLASH->KEYR = FLASH_KEY2;
    FLASH->CR &= ~FLASH_CR_LOCK;
    chSysUnlock();
}

void flash_lock(void)
{
    chSysLock();
    while (FLASH->SR & FLASH_SR_BSY);
    FLASH->CR |= FLASH_CR_LOCK;
    chSysUnlock();
}


void flash_erase(void)
{
    chSysLock();
    while (FLASH->SR & FLASH_SR_BSY);
    FLASH->CR &= ~FLASH_CR_SNB;
    FLASH->CR |= JOURNAL_FLASH_SECTOR << FLASH_CR_SNB_Pos;
    FLASH->CR |= FLASH_CR_SER;
    FLASH->CR &= ~FLASH_CR_PSIZE;
    FLASH->CR |= FLASH_CR_PSIZE_1;
    FLASH->CR &= ~FLASH_CR_PG;
    FLASH->CR |= FLASH_CR_STRT;
    while (FLASH->SR & FLASH_SR_BSY);
    chSysUnlock();
}

void flash_write(uint8_t *addr, uint8_t val)
{
    chSysLock();
    while (FLASH->SR & FLASH_SR_BSY);
    FLASH->CR &= ~FLASH_CR_PSIZE;
    FLASH->CR &= ~FLASH_CR_SER;
    FLASH->CR |= FLASH_CR_PG;
    *addr = val;
    while (FLASH->SR & FLASH_SR_BSY);
    FLASH->CR &= ~FLASH_CR_PG;
    chSysUnlock();
}

void journal_erase(void)
{
    flash_unlock();
    flash_erase();
    flash_lock();
    i = -1;
}

uint8_t journal_read_value(void)
{
    if (i < 0)
    {
        for (i = 0; (i < JOURNAL_SIZE) && (journal[i] != 0xff); ++i);
        i--;
        if (i == -1)
            return JOURNAL_EMPTY;
    }
    return journal[i];
}

void journal_write_value(uint8_t val)
{
    chDbgAssert(val != JOURNAL_EMPTY, "val is empty");
    uint8_t v = 0;

    if (i != -1)
    {
        v = journal_read_value();
        if ((v & val) != val)
        {
            if (i == (JOURNAL_SIZE-1))
                journal_erase();
            ++i;
        }
    }
    else
        ++i;
    flash_unlock();
    flash_write(journal + i, val);
    flash_lock();
}
