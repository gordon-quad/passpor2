/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _USBHID_H_
#define _USBHID_H_

#include "hal.h"

/*
 * Keycode definitions as present in the usb hid usage tables, and as available
 * as valid key events for linux.
 *
 * Refer to
 * linux: source/drivers/hid/hid-input.c
 * usb hid usage tables v 1.12
 */
enum hid_keyboard_keypad_usage {
    EVENT_NONE = 0,
    EVENT_ERRORROLLOVER,
    EVENT_POSTFAIL,
    EVENT_ERRORUNDEFINED,
    KEY_A,
    KEY_B,
    KEY_C,
    KEY_D,
    KEY_E,
    KEY_F,
    KEY_G,
    KEY_H,
    KEY_I,
    KEY_J,
    KEY_K,
    KEY_L,
    KEY_M,
    KEY_N,
    KEY_O,
    KEY_P,
    KEY_Q,
    KEY_R,
    KEY_S,
    KEY_T,
    KEY_U,
    KEY_V,
    KEY_W,
    KEY_X,
    KEY_Y,
    KEY_Z,
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,
    KEY_0,
    KEY_ENTER,
    KEY_ESC,
    KEY_BACKSPACE,
    KEY_TAB,
    KEY_SPACE,
    KEY_MINUS,
    KEY_EQUAL,
    KEY_LEFTBRACE,
    KEY_RIGHTBRACE,
    KEY_BACKSLASH,
    KEY_NUMBER,
    KEY_SEMICOLON,
    KEY_QUOTE,
    KEY_BACKTICK,
    KEY_COMMA,
    KEY_PERIOD,
    KEY_SLASH,
    KEY_CAPS_LOCK,
    KEY_F1,
    KEY_F2,
    KEY_F3,
    KEY_F4,
    KEY_F5,
    KEY_F6,
    KEY_F7,
    KEY_F8,
    KEY_F9,
    KEY_F10,
    KEY_F11,
    KEY_F12,
    KEY_PRINTSCREEN,
    KEY_SCROLL_LOCK,
    KEY_PAUSE,
    KEY_INSERT,
    KEY_HOME,
    KEY_PAGE_UP,
    KEY_DELETE,
    KEY_END,
    KEY_PAGE_DOWN,
    KEY_RIGHT,
    KEY_LEFT,
    KEY_DOWN,
    KEY_UP,
    KEY_NUM_LOCK,
    KEY_PAD_SLASH,
    KEY_PAD_ASTERISK,
    KEY_PAD_MINUS,
    KEY_PAD_PLUS,
    KEY_PAD_ENTER,
    KEY_PAD_1,
    KEY_PAD_2,
    KEY_PAD_3,
    KEY_PAD_4,
    KEY_PAD_5,
    KEY_PAD_6,
    KEY_PAD_7,
    KEY_PAD_8,
    KEY_PAD_9,
    KEY_PAD_0,
    KEY_PAD_PERIOD,
    KEY_ISO_SLASH,
    KEY_APP,
    EVENT_STATUS,
    KEY_PAD_EQUAL,
    KEY_F13,
    KEY_F14,
    KEY_F15,
    KEY_F16,
    KEY_F17,
    KEY_F18,
    KEY_F19,
    KEY_F20,
    KEY_F21,
    KEY_F22,
    KEY_F23,
    KEY_F24,
    KEY_EXEC,
    KEY_HELP,
    KEY_MENU,
    KEY_SELECT,
    KEY_STOP,
    KEY_AGAIN,
    KEY_UNDO,
    KEY_CUT,
    KEY_COPY,
    KEY_PASTE,
    KEY_FIND,
    KEY_MUTE,
    KEY_VOL_UP,
    KEY_VOL_DOWN,
    KEY_CAPS_TLOCK,
    KEY_NUM_TLOCK,
    KEY_SCROLL_TLOCK,
    KEY_PAD_COMMA, /* Brazilian keypad period */
    KEY_PAD_EQUAL_AS, /* AS/400 Keyboard */
    KEY_INTER1, /* KANJI1 - Brazillian and Japanese "Ru" and "-" */
    KEY_INTER2, /* KANJI2 - Japanese Katakana/Hiragana */
    KEY_INTER3, /* KANJI3 - Japanese Yen */
    KEY_INTER4, /* KANJI4 - Japanese Henkan */
    KEY_INTER5, /* KANJI5 - Japanese Muhenkan */
    KEY_INTER6, /* KANJI6 - PC('USB', 0x62) Comma (Ka-m-ma) */
    KEY_INTER7, /* KANJI7 - Double-Byte/Single-Byte Toggle */
    KEY_INTER8, /* KANJI8 - Undefined */
    KEY_INTER9, /* KANJI9 - Undefined */
    KEY_LANG1, /* Korean Hangul/English Toggle */
    KEY_LANG2, /* Korean Hanja Conversion - Japanese Eisu */
    KEY_LANG3, /* Japanese Katakana Key (USB) */
    KEY_LANG4, /* Japanese Hiragana Key (USB) */
    KEY_LANG5, /* Japanese Zenkaku/Hankaku Key (USB) */
    KEY_LANG6, /* Reserved (Application Specific) */
    KEY_LANG7, /* Reserved (Application Specific) */
    KEY_LANG8, /* Reserved (Application Specific) */
    KEY_LANG9, /* Reserved (Application Specific) */
    KEY_ALT_ERASE, /* Special Erase (See Spec) */
    KEY_SYSREQ_ATT, /* Modifier Type */
    KEY_CANCEL,
    KEY_CLEAR,
    KEY_PRIOR,
    KEY_RETURN,
    KEY_SEPARATOR,
    KEY_OUT,
    KEY_OPER,
    KEY_CLEAR_AGAIN,
    KEY_CRSEL_PROPS,
    KEY_EXSEL,

    /* 0xa5 - 0xaf Reserved */

    KEY_PAD_00 = 0xB0,
    KEY_PAD_000,
    KEY_1000_SEP,
    KEY_DECIMAL_SEP,
    KEY_CURRENCY_MAIN,
    KEY_CURRENCY_SUB,
    KEY_PAD_LPAREN,
    KEY_PAD_RPAREN,
    KEY_PAD_LBRACE,
    KEY_PAD_RBRACE,
    KEY_PAD_TAB,
    KEY_PAD_BACKSPACE,
    KEY_PAD_A,
    KEY_PAD_B,
    KEY_PAD_C,
    KEY_PAD_D,
    KEY_PAD_E,
    KEY_PAD_F,
    KEY_PAD_XOR,
    KEY_PAD_CHEVRON,
    KEY_PAD_PERCENT,
    KEY_PAD_LTHAN,
    KEY_PAD_GTHAN,
    KEY_PAD_BITAND,
    KEY_PAD_AND,
    KEY_PAD_BITOR,
    KEY_PAD_OR,
    KEY_PAD_COLON,
    KEY_PAD_POUND,
    KEY_PAD_SPACE,
    KEY_PAD_AT,
    KEY_PAD_EXCLAIM,
    KEY_PAD_MEM_STORE,
    KEY_PAD_MEM_RECALL,
    KEY_PAD_MEM_CLEAR,
    KEY_PAD_MEM_ADD,
    KEY_PAD_MEM_SUB,
    KEY_PAD_MEM_MULT,
    KEY_PAD_MEM_DIV,
    KEY_PAD_PLUS_MINUS,
    KEY_PAD_CLEAR,
    KEY_PAD_CLEAR_ENTRY,
    KEY_PAD_BINARY,
    KEY_PAD_OCTAL,
    KEY_PAD_DECIMAL,
    KEY_PAD_HEX,

    /* 0xde - 0xdf = Reserved */

    KEY_LCTRL = 0xE0,
    KEY_LSHIFT,
    KEY_LALT,
    KEY_LGUI,
    KEY_RCTRL,
    KEY_RSHIFT,
    KEY_RALT,
    KEY_RGUI,

    /* 0xe8 - 0xff = Reserved */
};

#define MODIFIER_BIT(mod) (1<<(mod & 0x07))

#define _SHIFT(key) ((MODIFIER_BIT(KEY_LSHIFT) << 8) | key)
#define _CTRL(key) ((MODIFIER_BIT(KEY_LCTRL) << 8) | key)
#define _ALT(key) ((MODIFIER_BIT(KEY_LALT) << 8) | key)

/*
 * HID specific constants
 */
#define USB_DESCRIPTOR_HID_REPORT 0x22
#define HID_GET_REPORT 0x01
#define HID_SET_REPORT 0x09
#define HID_DESCRIPTOR_OFFSET 67


#define USB_HID_REPORT_SIZE 8
#define USB_HID_BUFFERS_NUMBER 8

typedef enum {
    USBHID_UNINIT = 0,
    USBHID_STOP = 1,
    USBHID_READY = 2
} usbhidstate_t;

typedef union {
    uint8_t raw[USB_HID_REPORT_SIZE];
    struct {
        uint8_t mods;
        uint8_t reserved;
        uint8_t keys[USB_HID_REPORT_SIZE-2];
    };
} __attribute__ ((packed)) report_keyboard_t;

typedef struct {
    USBDriver *usbp;
    usbep_t int_in;
} USBHIDConfig;

typedef struct {
    usbhidstate_t state;
    output_buffers_queue_t obqueue;
    uint8_t ob[BQ_BUFFER_SIZE(USB_HID_BUFFERS_NUMBER,
                              USB_HID_REPORT_SIZE)];
    const USBHIDConfig *config;
} USBHIDDriver;

void usbhidInit(void);
void usbhidObjectInit(USBHIDDriver *usbhidp);
void usbhidStart(USBHIDDriver *usbhidp, const USBHIDConfig *config);
void usbhidStop(USBHIDDriver *usbhidp);
void usbhidSuspendHookI(USBHIDDriver *usbhidp);
void usbhidWakeupHookI(USBHIDDriver *usbhidp);
void usbhidConfigureHookI(USBHIDDriver *usbhidp);
bool usbhidRequestsHook(USBDriver *usbp);
void usbhidDataTransmitted(USBDriver *usbp, usbep_t ep);
msg_t usbhidSendKey(USBHIDDriver *usbhidp, uint16_t key);
msg_t usbhidReleaseKeys(USBHIDDriver *usbhidp);

#endif//_USBHID_H_
