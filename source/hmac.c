/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>

#include "sha1.h"
#include "hmac.h"

/* Note that this function is NOT thread-safe */
void hmac_sha1(const uint8_t *text, size_t text_len, const uint8_t *key, size_t key_len, uint8_t *digest)
{
    SHA1Context sha1_context;
    uint8_t k_ipad[SHA1BlockSize];
    uint8_t k_opad[SHA1BlockSize];
    uint8_t tk[SHA1BlockSize];
    int i;

    if (key_len > SHA1BlockSize)
    {
        SHA1Reset(&sha1_context);
        SHA1Input(&sha1_context, key, key_len);
        SHA1Result(&sha1_context, tk);
        key = tk;
        key_len = SHA1HashSize;
    }
    memset(k_ipad, 0, SHA1BlockSize);
    memset(k_opad, 0, SHA1BlockSize);
    memmove(k_ipad, key, key_len);
    memmove(k_opad, key, key_len);

    for (i=0; i<SHA1BlockSize; i++)
    {
        k_ipad[i] ^= 0x36;
        k_opad[i] ^= 0x5c;
    }
    SHA1Reset(&sha1_context);
    /* perform inner sha1 */
    SHA1Input(&sha1_context, k_ipad, SHA1BlockSize);
    SHA1Input(&sha1_context, text, text_len);
    SHA1Result(&sha1_context, digest);
    SHA1Reset(&sha1_context);
    /* perform outer sha1 */
    SHA1Input(&sha1_context, k_opad, SHA1BlockSize);
    SHA1Input(&sha1_context, digest, SHA1HashSize);
    /* store the result */
    SHA1Result(&sha1_context, digest);
}
