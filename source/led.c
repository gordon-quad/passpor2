/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "hal.h"
#include "led.h"

#define LED_MB_SIZE 4

static msg_t led_red_mb_buffer[LED_MB_SIZE];
static MAILBOX_DECL(led_red_mb, led_red_mb_buffer, LED_MB_SIZE);
static msg_t led_green_mb_buffer[LED_MB_SIZE];
static MAILBOX_DECL(led_green_mb, led_green_mb_buffer, LED_MB_SIZE);

static mailbox_t *led_mbs[] = {&led_red_mb, &led_green_mb};

static THD_WORKING_AREA(led_red_wa, 128);
static THD_WORKING_AREA(led_green_wa, 128);

static ioline_t led_lines[] = {LINE_LED_RED, LINE_LED_GREEN};

THD_FUNCTION(led_thread, p)
{
    unsigned int led = (unsigned int)p;
    sysinterval_t timeout = TIME_INFINITE;
    int8_t num_flashes = 0;

    if (p == LED_RED)
        chRegSetThreadName("led_red");
    else
        chRegSetThreadName("led_green");

    palClearLine(led_lines[led]);

    while (1)
    {
        msg_t msg;
        if (chMBFetchTimeout(led_mbs[led], &msg, timeout) == MSG_OK)
        {
            switch (msg)
            {
            case LED_MODE_ON:
                timeout = TIME_INFINITE;
                num_flashes = 0;
                palSetLine(led_lines[led]);
                break;
            case LED_MODE_OFF:
                timeout = TIME_INFINITE;
                num_flashes = 0;
                palClearLine(led_lines[led]);
                break;
            case LED_MODE_SINGLE_SHORT_FLASH:
                timeout = LED_SHORT_DELAY;
                num_flashes = 0;
                palSetLine(led_lines[led]);
                break;
            case LED_MODE_SINGLE_LONG_FLASH:
                timeout = LED_LONG_DELAY;
                num_flashes = 0;
                palSetLine(led_lines[led]);
                break;
            case LED_MODE_SHORT_FLASH:
                timeout = LED_SHORT_DELAY;
                num_flashes = LED_NUM_FLASHES;
                palSetLine(led_lines[led]);
                break;
            case LED_MODE_LONG_FLASH:
                timeout = LED_LONG_DELAY;
                num_flashes = LED_NUM_FLASHES;
                palSetLine(led_lines[led]);
                break;
            case LED_MODE_BLINK_FAST:
                timeout = LED_SHORT_DELAY;
                num_flashes = -1;
                palSetLine(led_lines[led]);
                break;
            case LED_MODE_BLINK_SLOW:
                timeout = LED_LONG_DELAY;
                num_flashes = -1;
                palSetLine(led_lines[led]);
                break;
            }
        }
        else
        {
            palToggleLine(led_lines[led]);
            if (num_flashes > 0)
                num_flashes--;
            else if (num_flashes == 0)
                timeout = TIME_INFINITE;
        }
    }
}

void led_set(uint8_t led, msg_t mode)
{
    chMBPostTimeout(led_mbs[led], mode, TIME_INFINITE);
}

void led_init(void)
{
    // Nothing to be done
}

void led_start(void)
{
    chThdCreateStatic(led_green_wa, sizeof(led_green_wa),
            NORMALPRIO + 10, led_thread, (void *)LED_GREEN);
    chThdCreateStatic(led_red_wa, sizeof(led_red_wa),
            NORMALPRIO + 10, led_thread, (void *)LED_RED);
}
