/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include "ch.h"
#include "pwd.h"
#include "util.h"
#include "hmac.h"
#include "masterkey.h"

#define MAC_STATIC_FMT "static:%04hx"
#define MAC_STATIC_LEN (6+1+4+1)

#define MAC_URL_FMT "url:%s:%04hx"
#define MAC_URL_LEN (4+1+MAX_URL_LEN+1+4+1)

#define MAC_OTP_FMT "otp:%04hx"
#define MAC_OTP_LEN (3+1+4+1)

void pwd_str(uint8_t *data, size_t data_len, char *dest)
{
    uint16_t data_idx[PWD_WORD_LEN];

    bit_split(data, data_len, DICT_BITS, data_idx, PWD_WORD_LEN);

    for(int i = 0; i < PWD_WORD_LEN; ++i)
    {
        memcpy(dest, &dict[WORD_LEN * data_idx[i]], 4);
        *dest = toupper(*dest);
        dest += 4;
        if (i != (PWD_WORD_LEN - 1))
        *(dest++) = PWD_SEP;
    }
    memcpy(dest, PWD_APPEND, PWD_APPEND_LEN);
}

void pwd_static(uint16_t c, char *out)
{
    char buf[MAC_STATIC_LEN];
    uint8_t stc[KEY_LEN];

    snprintf(buf, MAC_STATIC_LEN, MAC_STATIC_FMT, c);
    hmac_sha1((uint8_t *)buf, strlen(buf), masterkey, KEY_LEN, stc);

    pwd_str(stc, KEY_LEN, out);
}

void pwd_url(char *url, uint16_t c, char *out)
{
    char buf[MAC_URL_LEN];
    uint8_t key[KEY_LEN];

    snprintf(buf, MAC_URL_LEN, MAC_URL_FMT, url, c);
    hmac_sha1((uint8_t *)buf, strlen(buf), masterkey, KEY_LEN, key);

    pwd_str(key, KEY_LEN, out);
}

void pwd_otp_key(uint16_t c, size_t key_len, char *out)
{
    char buf[MAC_OTP_LEN];
    uint8_t otp[KEY_LEN];

    snprintf(buf, MAC_OTP_LEN, MAC_OTP_FMT, c);
    hmac_sha1((uint8_t *)buf, strlen(buf), masterkey, KEY_LEN, otp);

    base32(otp, key_len, out);
}

void pwd_otp_code(uint16_t c, size_t key_len, uint64_t data, char *out)
{
    char buf[MAC_OTP_LEN];
    uint8_t otp[KEY_LEN];
    uint8_t res[KEY_LEN];
    uint8_t pmsg[8];
    uint8_t offset;
    uint32_t code;

    snprintf(buf, MAC_OTP_LEN, MAC_OTP_FMT, c);
    hmac_sha1((uint8_t *)buf, strlen(buf), masterkey, KEY_LEN, otp);

    // convert to big-endian
    pmsg[0] = 0xff & (data >> 56);
    pmsg[1] = 0xff & (data >> 48);
    pmsg[2] = 0xff & (data >> 40);
    pmsg[3] = 0xff & (data >> 32);
    pmsg[4] = 0xff & (data >> 24);
    pmsg[5] = 0xff & (data >> 16);
    pmsg[6] = 0xff & (data >>  8);
    pmsg[7] = 0xff & (data >>  0);

    hmac_sha1(pmsg, 8, otp, key_len, res);
    offset = res[KEY_LEN - 1] & 0x0f;
    code = (((res[offset] & 0x7f) << 24) |
            ((res[offset + 1] & 0xff) << 16) |
            ((res[offset + 2] & 0xff) << 8) |
            ((res[offset + 3] & 0xff)));

    code = code % 1000000;

    snprintf(out, 7, "%06ld", code);
}

