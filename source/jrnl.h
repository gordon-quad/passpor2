/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _JRNL_H_
#define _JRNL_H_

#define JOURNAL_EMPTY         0xff

#define JOURNAL_ADDRESS       0x0800c000
#define JOURNAL_SIZE          16384
#define JOURNAL_FLASH_SECTOR  3

void journal_erase(void);
uint8_t journal_read_value(void);
void journal_write_value(uint8_t val);

#endif//_JRNL_H_
