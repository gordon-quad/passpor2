/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#include "ch.h"
#include "hal.h"
#include "shell.h"
#include "shell_cmd.h"
#include "chprintf.h"

#include "usbhid.h"
#include "usbcfg.h"
#include "sha1.h"
#include "hmac.h"
#include "util.h"
#include "dict.h"
#include "led.h"
#include "typer.h"
#include "btn.h"
#include "ctrl.h"
#include "pwd.h"
#include "btldr.h"
#include "auth.h"
#include "jrnl.h"


void cmd_genpass(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint16_t cntr = 0;
    unsigned long c;
    char *ptr;
    char pwd[PWD_LEN];
    if (argc != 2)
    {
        shellUsage(chp, "genpass counter url");
        return;
    }

    c = strtoul(argv[0], &ptr, 0);
    if (*ptr != '\0')
    {
        chprintf(chp, "first argument must be valid integer!\r\n");
        return;
    }

    if (c > 65535)
    {
        chprintf(chp, "first argument must be less than 65536!\r\n");
        return;
    }

    if (strlen(argv[1]) > MAX_URL_LEN)
    {
        chprintf(chp, "second paramenetr is too long!\r\n");
        return;
    }

    cntr = c;
    pwd_url(argv[1], cntr, pwd);
    ctrl_arm(pwd, PWD_LEN);
}

void cmd_static(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint16_t cntr = 0;
    unsigned long c;
    char *ptr;
    char pwd[PWD_LEN];
    if (argc != 1)
    {
        shellUsage(chp, "static counter");
        return;
    }

    c = strtoul(argv[0], &ptr, 0);
    if (*ptr != '\0')
    {
        chprintf(chp, "first argument must be valid integer!\r\n");
        return;
    }

    if (c > 65535)
    {
        chprintf(chp, "first argument must be less than 65536!\r\n");
        return;
    }

    cntr = c;
    pwd_static(cntr, pwd);
    ctrl_arm(pwd, PWD_LEN);
}

void cmd_otp80_key(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint16_t cntr = 0;
    unsigned long c;
    char *ptr;
    char pwd[OTP80_KEY_LEN/5 * 8];

    if (!auth_is_admin_unlocked())
    {
        chprintf(chp, "available only in admin mode\r\n");
        return;
    }

    if (argc != 1)
    {
        shellUsage(chp, "otp80_key counter");
        return;
    }

    c = strtoul(argv[0], &ptr, 0);
    if (*ptr != '\0')
    {
        chprintf(chp, "first argument must be valid integer!\r\n");
        return;
    }

    if (c > 65535)
    {
        chprintf(chp, "first argument must be less than 65536!\r\n");
        return;
    }

    cntr = c;
    pwd_otp_key(cntr, OTP80_KEY_LEN, pwd);
    ctrl_arm(pwd, OTP80_KEY_LEN/5 * 8);
}

void cmd_otp80_code(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint16_t cntr = 0;
    uint64_t data = 0;
    unsigned long long c;
    char *ptr;
    char pwd[OTP_CODE6_LEN];
    if (argc != 2)
    {
        shellUsage(chp, "otp80_code counter data");
        return;
    }

    c = strtoul(argv[0], &ptr, 0);
    if (*ptr != '\0')
    {
        chprintf(chp, "first argument must be valid integer!\r\n");
        return;
    }

    if (c > 65535)
    {
        chprintf(chp, "first argument must be less than 65536!\r\n");
        return;
    }

    cntr = c;

    c = strtoull(argv[1], &ptr, 0);
    if (*ptr != '\0')
    {
        chprintf(chp, "second argument must be valid integer!\r\n");
        return;
    }

    data = c;

    pwd_otp_code(cntr, OTP80_KEY_LEN, data, pwd);
    ctrl_arm(pwd, OTP_CODE6_LEN);
}

void cmd_otp160_key(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint16_t cntr = 0;
    unsigned long c;
    char *ptr;
    char pwd[OTP160_KEY_LEN/5 * 8];

    if (!auth_is_admin_unlocked())
    {
        chprintf(chp, "available only in admin mode\r\n");
        return;
    }

    if (argc != 1)
    {
        shellUsage(chp, "otp160_key counter");
        return;
    }

    c = strtoul(argv[0], &ptr, 0);
    if (*ptr != '\0')
    {
        chprintf(chp, "first argument must be valid integer!\r\n");
        return;
    }

    if (c > 65535)
    {
        chprintf(chp, "first argument must be less than 65536!\r\n");
        return;
    }

    cntr = c;
    pwd_otp_key(cntr, OTP160_KEY_LEN, pwd);
    ctrl_arm(pwd, OTP160_KEY_LEN/5 * 8);
}

void cmd_otp160_code(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint16_t cntr = 0;
    uint64_t data = 0;
    unsigned long long c;
    char *ptr;
    char pwd[OTP_CODE6_LEN];
    if (argc != 2)
    {
        shellUsage(chp, "otp160_code counter data_num");
        return;
    }

    c = strtoul(argv[0], &ptr, 0);
    if (*ptr != '\0')
    {
        chprintf(chp, "first argument must be valid integer!\r\n");
        return;
    }

    if (c > 65535)
    {
        chprintf(chp, "first argument must be less than 65536!\r\n");
        return;
    }

    cntr = c;

    c = strtoull(argv[1], &ptr, 0);
    if (*ptr != '\0')
    {
        chprintf(chp, "second argument must be valid integer!\r\n");
        return;
    }

    data = c;

    pwd_otp_code(cntr, OTP160_KEY_LEN, data, pwd);
    ctrl_arm(pwd, OTP_CODE6_LEN);
}


void cmd_dfu(BaseSequentialStream *chp, int argc, char *argv[])
{
    (void)argv;

    if (!auth_is_admin_unlocked())
    {
        chprintf(chp, "available only in admin mode\r\n");
        return;
    }

    if (argc != 0)
    {
        shellUsage(chp, "dfu");
        return;
    }

    usbDisconnectBus(&USBD1);
    chThdSleepMilliseconds(1000);

    btldr_boot_bootloader();
}

void cmd_puc(BaseSequentialStream *chp, int argc, char *argv[])
{
    if (auth_is_admin_unlocked())
    {
        chprintf(chp, "unlocked!\r\n");
        return;
    }
    else if (argc == 0)
    {
        chprintf(chp, "locked!\r\n");
        return;
    }

    if (argc != 1)
    {
        shellUsage(chp, "puc [code]");
        return;
    }

    auth_admin_unlock(argv[0]);
    if (auth_is_admin_unlocked())
        chprintf(chp, "unlocked!\r\n");
    else
        chprintf(chp, "PUC is incorrect!\r\n");
}

/* shell commands for debug, can be disabled in production build */
#ifdef _DEBUG

void cmd_sha1(BaseSequentialStream *chp, int argc, char *argv[])
{
    (void)argv;
    if (argc != 1)
    {
        shellUsage(chp, "sha1 \"text to be hashed\"");
        return;
    }
    SHA1Context sha;
    int i, err;
    uint8_t digest[SHA1HashSize];
    err = SHA1Reset(&sha);
    if (err)
    {
        chprintf(chp, "SHA1Reset Error: %d" SHELL_NEWLINE_STR, err);
        return;
    }
    err = SHA1Input(&sha, (uint8_t *) argv[0], strlen(argv[0]));
    if (err)
    {
        chprintf(chp, "SHA1Input Error: %d", SHELL_NEWLINE_STR, err);
        return;
    }
    err = SHA1Result(&sha, digest);
    if (err)
    {
        chprintf(chp, "SHA1Result Error: %d", SHELL_NEWLINE_STR, err);
        return;
    }
    for(i = 0; i < SHA1HashSize ; ++i)
    {
        chprintf(chp, "%02X", digest[i]);
    }
    chprintf(chp, SHELL_NEWLINE_STR);
}

void cmd_hmac_sha1(BaseSequentialStream *chp, int argc, char *argv[])
{
    if (argc != 2)
    {
        shellUsage(chp, "hmac_sha1 \"text to be hashed\" \"key\"");
        return;
    }
    uint8_t digest[SHA1HashSize];
    int i;
    hmac_sha1((uint8_t *)argv[0], strlen(argv[0]), (uint8_t *)argv[1], strlen(argv[1]), digest);
    for(i = 0; i < SHA1HashSize ; ++i)
    {
        chprintf(chp, "%02X", digest[i]);
    }
    chprintf(chp, SHELL_NEWLINE_STR);
}

void cmd_usbhid(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint16_t code;
    if (argc != 1)
    {
        shellUsage(chp, "usbhid code_to_send");
        return;
    }

    code = strtol(argv[0], NULL, 0);
    usbhidSendKey(&USBHID1, code);
    usbhidReleaseKeys(&USBHID1);
}

void print_bits(BaseSequentialStream *chp, int len, uint16_t d)
{
    for(int i=len-1; i >= 0; i--)
        (d & (1 << i)) ? chprintf(chp, "1") : chprintf(chp, "0");
    chprintf(chp, " ");
}

static uint8_t randombuf[16] = {0x5f, 0x4c, 0xb0, 0xf8, 0x3d, 0x61, 0xf4, 0xf8, 0xc2, 0x94, 0x65, 0x18, 0xc9, 0xd1, 0xc5, 0xe6};

void cmd_bitsplit(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint8_t num_bits;

    if (argc != 1)
    {
        shellUsage(chp, "bitsplit bits");
        return;
    }

    num_bits = strtol(argv[0], NULL, 0);
    size_t dest_len = 16 * 8 / num_bits;

    if ((dest_len * num_bits / 8) < 16)
        ++dest_len;

    uint16_t output[dest_len];

    chprintf(chp, "input: ");
    for (unsigned int i = 0; i < 16; ++i)
        print_bits(chp, 8, randombuf[i]);
    chprintf(chp, "\r\nnum_bits = %d\r\ndest_len = %d\r\n", num_bits, dest_len);

    bit_split(randombuf, 16, num_bits, output, dest_len);

    chprintf(chp, "output: ");
    for (unsigned int i = 0; i < dest_len; ++i)
        print_bits(chp, num_bits, output[i]);
    chprintf(chp, "\r\n");

}

void cmd_pwdict(BaseSequentialStream *chp, int argc, char *argv[])
{
    (void) argv;
    if (argc != 0)
    {
        shellUsage(chp, "pwdict");
        return;
    }

    char output[PWD_LEN+1];
    pwd_str(randombuf, 16, output);
    output[PWD_LEN] = '\0';

    chprintf(chp, "password: %s\r\n", output);
}

void cmd_led(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint8_t led;
    msg_t mode;
    if (argc != 2)
    {
        shellUsage(chp, "led <r|g> <0|1|f|F|s|S|b|B>");
        return;
    }

    switch (argv[0][0])
    {
    case 'r':
        led = LED_RED;
        break;
    case 'g':
        led = LED_GREEN;
        break;
    }

    switch (argv[1][0])
    {
    case '0':
        mode = LED_MODE_OFF;
        break;
    case '1':
        mode = LED_MODE_ON;
        break;
    case 'f':
        mode = LED_MODE_SHORT_FLASH;
        break;
    case 'F':
        mode = LED_MODE_LONG_FLASH;
        break;
    case 's':
        mode = LED_MODE_SINGLE_SHORT_FLASH;
        break;
    case 'S':
        mode = LED_MODE_SINGLE_LONG_FLASH;
        break;
    case 'b':
        mode = LED_MODE_BLINK_FAST;
        break;
    case 'B':
        mode = LED_MODE_BLINK_SLOW;
        break;
    }

    led_set(led, mode);
}

void cmd_type(BaseSequentialStream *chp, int argc, char *argv[])
{
    if (argc != 1)
    {
        shellUsage(chp, "type string");
        return;
    }

    chprintf(chp, "Sending string \"%s\"...\r\n", argv[0]);

    typer_type(argv[0], strlen(argv[0]));
}

void cmd_btn(BaseSequentialStream *chp, int argc, char *argv[])
{
    (void)argv;
    systime_t start;
    event_listener_t btn0_lst, btn1_lst;
    if (argc != 0)
    {
        shellUsage(chp, "btn");
        return;
    }

    chEvtRegister(&btn_short_press_evt, &btn0_lst, 0);
    chEvtRegister(&btn_long_press_evt, &btn1_lst, 1);
    chprintf(chp, "Waiting for button events for next 60 secs...\r\n");
    start = chVTGetSystemTimeX();

    while(chVTIsSystemTimeWithinX(start, start + TIME_S2I(60)))
    {
        eventmask_t events;
        events = chEvtWaitOneTimeout(ALL_EVENTS, TIME_S2I(1));
        if (events & EVENT_MASK(0))
            chprintf(chp, "Detected short press!\r\n");
        if (events & EVENT_MASK(1))
            chprintf(chp, "Detected long press!\r\n");
    }

    chEvtUnregister(&btn_short_press_evt, &btn0_lst);
    chEvtUnregister(&btn_long_press_evt, &btn1_lst);
}

void cmd_arm(BaseSequentialStream *chp, int argc, char *argv[])
{
    if (argc != 1)
    {
        shellUsage(chp, "arm string");
        return;
    }

    ctrl_arm(argv[0], strlen(argv[0]));
}

void cmd_base32(BaseSequentialStream *chp, int argc, char *argv[])
{
    if (argc != 1)
    {
        shellUsage(chp, "base32 string");
        return;
    }

    size_t len = strlen(argv[0]);

    if (len % 5 != 0)
    {
        chprintf(chp, "base32 support no padding, len must be multiple of 5\r\n");
        return;
    }
    char out[len / 5 * 8+ 1];

    base32((uint8_t *)argv[0], len, out);
    out[len / 5 * 8 + 1] = '\0';

    chprintf(chp, "%s\r\n", out);
}

void cmd_jrnl_erase(BaseSequentialStream *chp, int argc, char *argv[])
{
    (void)argv;
    if (argc != 0)
    {
        shellUsage(chp, "jrnl_erase");
        return;
    }

    journal_erase();
    chprintf(chp, "erased!\r\n");
}

void cmd_jrnl_read(BaseSequentialStream *chp, int argc, char *argv[])
{
    (void)argv;
    if (argc != 0)
    {
        shellUsage(chp, "jrnl_read");
        return;
    }

    chprintf(chp, "journal value = 0x%02x\r\n", journal_read_value());
}

void cmd_jrnl_write(BaseSequentialStream *chp, int argc, char *argv[])
{
    uint32_t c;
    uint8_t val;
    char *ptr;
    if (argc != 1)
    {
        shellUsage(chp, "jrnl_write value");
        return;
    }

    c = strtoul(argv[0], &ptr, 0);
    if (*ptr != '\0')
    {
        chprintf(chp, "first argument must be valid integer!\r\n");
        return;
    }

    if (c > 255)
    {
        chprintf(chp, "first argument must be less than 255!\r\n");
        return;
    }

    val = c;

    journal_write_value(val);
    chprintf(chp, "written!\r\n");
}

#endif  /* _DEBUG */

static const ShellCommand commands[] = {
    {"genpass", cmd_genpass},
    {"static", cmd_static},
    {"otp80_key", cmd_otp80_key},
    {"otp80_code", cmd_otp80_code},
    {"otp160_key", cmd_otp160_key},
    {"otp160_code", cmd_otp160_code},
    {"dfu", cmd_dfu},
    {"puc", cmd_puc},
#ifdef _DEBUG
    {"sha1", cmd_sha1},
    {"hmac_sha1", cmd_hmac_sha1},
    {"usbhid", cmd_usbhid},
    {"bitsplit", cmd_bitsplit},
    {"pwdict", cmd_pwdict},
    {"led", cmd_led},
    {"type", cmd_type},
    {"btn", cmd_btn},
    {"arm", cmd_arm},
    {"base32", cmd_base32},
    {"jrnl_write", cmd_jrnl_write},
    {"jrnl_read", cmd_jrnl_read},
    {"jrnl_erase", cmd_jrnl_erase},
#endif  /* _DEBUG */
    {NULL, NULL}
};

const ShellConfig shell_cfg = {
    (BaseSequentialStream *)&SDU1,
    commands
};

