/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "btldr.h"

void btldr_boot_bootloader(void)
{
    BOOTLOADER_INDICATOR = BOOTLOADER_MAGIC;

    NVIC_SystemReset();
}

void btldr_check_bootloader(void)
{
    if (BOOTLOADER_INDICATOR == BOOTLOADER_MAGIC)
    {
        BOOTLOADER_INDICATOR = 0x0;
        SYSCFG->MEMRMP = 0x01;
        SCB->VTOR = 0x0;
        __set_CONTROL(0x0);
        __DSB();
        __ISB();
        __set_MSP(*(volatile uint32_t *)BOOTLOADER_ADDR);
        __enable_irq();
        ((void (*)(void)) *((volatile uint32_t*) (BOOTLOADER_ADDR+4)))();
        while (1);
    }
}
