/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _LED_H_
#define _LED_H_

#define LED_RED                        0
#define LED_GREEN                      1

#define LED_MODE_OFF                   0x08000000
#define LED_MODE_ON                    0x08000001
#define LED_MODE_SHORT_FLASH           0x08000002
#define LED_MODE_LONG_FLASH            0x08000003
#define LED_MODE_SINGLE_SHORT_FLASH    0x08000004
#define LED_MODE_SINGLE_LONG_FLASH     0x08000005
#define LED_MODE_BLINK_SLOW            0x08000006
#define LED_MODE_BLINK_FAST            0x08000007

#define LED_NUM_FLASHES                4

#define LED_SHORT_DELAY                TIME_MS2I(100)
#define LED_LONG_DELAY                 TIME_MS2I(500)

#define LED_POLL_DELAY                 TIME_MS2I(20)

#define LED_WA_SIZE THD_WORKING_AREA_SIZE(128)

void led_init(void);
void led_start(void);
void led_set(uint8_t led, msg_t mode);

#endif//_LED_H_
