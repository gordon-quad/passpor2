/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _BTN_H_
#define _BTN_H_

#define LONG_PRESS_DURATION  TIME_MS2I(500)
#define DEBOUNCE_FILTER_TIME   TIME_MS2I(10)

#define CLICK_SERIES_TIMEOUT TIME_MS2I(500)

void btn_init(void);
void btn_start(void);

extern event_source_t btn_short_press_evt;
extern event_source_t btn_long_press_evt;

#endif//_BTN_H_
