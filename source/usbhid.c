/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>

#include "hal.h"
#include "usbhid.h"

/*
 * Notification of filled buffer
 */
static void obnotify(io_buffers_queue_t *bqp)
{
    USBHIDDriver *usbhidp = bqGetLinkX(bqp);
    size_t n;

    if ((usbGetDriverStateI(usbhidp->config->usbp) != USB_ACTIVE) &&
        (usbhidp->state != USBHID_READY))
        return;

    if (!usbGetTransmitStatusI(usbhidp->config->usbp, usbhidp->config->int_in))
    {
        uint8_t *buf = obqGetFullBufferI(&usbhidp->obqueue, &n);
        chDbgAssert(buf != NULL, "buffer not found");
        chDbgAssert(n == USB_HID_REPORT_SIZE, "buffer size is not report size");
        usbStartTransmitI(usbhidp->config->usbp, usbhidp->config->int_in, buf, n);
    }
}

void usbhidInit(void)
{
    // Nothing to be done
}

void usbhidObjectInit(USBHIDDriver *usbhidp)
{
    usbhidp->state = USBHID_STOP;
    obqObjectInit(&usbhidp->obqueue, true, usbhidp->ob,
            USB_HID_REPORT_SIZE, USB_HID_BUFFERS_NUMBER,
            obnotify, usbhidp);
}

void usbhidStart(USBHIDDriver *usbhidp, const USBHIDConfig *config)
{
    USBDriver *usbp = config->usbp;

    osalDbgCheck(usbhidp != NULL);

    osalSysLock();
    osalDbgAssert((usbhidp->state == USBHID_STOP) || (usbhidp->state == USBHID_READY),
            "invalid state");
    usbp->in_params[config->int_in - 1U]   = usbhidp;

    usbhidp->config = config;
    usbhidp->state = USBHID_READY;
    osalSysUnlock();
}

void usbhidStop(USBHIDDriver *usbhidp)
{
    USBDriver *usbp = usbhidp->config->usbp;

    osalDbgCheck(usbhidp != NULL);

    osalSysLock();

    osalDbgAssert((usbhidp->state == USBHID_STOP) || (usbhidp->state == USBHID_READY),
            "invalid state");

    /* Driver in stopped state.*/
    usbp->in_params[usbhidp->config->int_in - 1U]  = NULL;
    usbhidp->config = NULL;
    usbhidp->state  = USBHID_STOP;

    /* Enforces a disconnection.*/
    obqResetI(&usbhidp->obqueue);
    osalOsRescheduleS();

    osalSysUnlock();
}

void usbhidSuspendHookI(USBHIDDriver *usbhidp)
{
    /* Avoiding events spam.*/
    if(bqIsSuspendedX(&usbhidp->obqueue)) {
        return;
    }
    bqSuspendI(&usbhidp->obqueue);
}

void usbhidWakeupHookI(USBHIDDriver *usbhidp)
{
    bqResumeX(&usbhidp->obqueue);
}

void usbhidConfigureHookI(USBHIDDriver *usbhidp)
{
    obqResetI(&usbhidp->obqueue);
    bqResumeX(&usbhidp->obqueue);
}

bool usbhidRequestsHook(USBDriver *usbp)
{
    // Handle HID class specific requests
    // Only GetReport is mandatory for HID devices
    if ((usbp->setup[0] & USB_RTYPE_TYPE_MASK) == USB_RTYPE_TYPE_CLASS)
    {
        if (usbp->setup[1] == HID_GET_REPORT)
        {
            /* setup[3] (MSB of wValue) = Report ID (must be 0 as we
             * have declared only one IN report)
             * setup[2] (LSB of wValue) = Report Type (1 = Input, 3 = Feature)
             */
            if ((usbp->setup[3] == 0) && (usbp->setup[2] == 1))
            {
                // Send empty hid report
                report_keyboard_t in_report;
                in_report.mods = 0;
                in_report.reserved = 0;
                in_report.keys[0] = 0;
                in_report.keys[1] = 0;
                in_report.keys[2] = 0;
                in_report.keys[3] = 0;
                in_report.keys[4] = 0;
                in_report.keys[5] = 0;

                usbSetupTransfer (usbp, (uint8_t *) &(in_report.raw),
                        USB_HID_REPORT_SIZE, NULL);

                return true;
            }
        }
        if (usbp->setup[1] == HID_SET_REPORT)
        {
            // Not implemented (yet)
            return true;
        }
    }
    return false;
}

void usbhidDataTransmitted(USBDriver *usbp, usbep_t ep)
{
    uint8_t *buf;
    size_t n;
    USBHIDDriver *usbhidp = usbp->in_params[ep - 1U];

    osalSysLockFromISR();

    obqReleaseEmptyBufferI(&usbhidp->obqueue);

    buf = obqGetFullBufferI(&usbhidp->obqueue, &n);

    if (buf != NULL)
    {
        chDbgAssert(n == USB_HID_REPORT_SIZE, "buffer size is not report size");
        usbStartTransmitI(usbp, ep, buf, n);
    }

    osalSysUnlockFromISR();
}

msg_t usbhidSendKey(USBHIDDriver *usbhidp, uint16_t key)
{
    msg_t res;
    report_keyboard_t report;
    report.mods = (key >> 8);
    report.reserved = 0;
    report.keys[0] = 0xff & key;
    report.keys[1] = 0;
    report.keys[2] = 0;
    report.keys[3] = 0;
    report.keys[4] = 0;
    report.keys[5] = 0;

    res = obqGetEmptyBufferTimeout(&usbhidp->obqueue, TIME_INFINITE);
    if (res != MSG_OK)
        return res;

    memcpy(usbhidp->obqueue.ptr, &report.raw, USB_HID_REPORT_SIZE);
    obqPostFullBuffer(&usbhidp->obqueue, USB_HID_REPORT_SIZE);

    return MSG_OK;
}

msg_t usbhidReleaseKeys(USBHIDDriver *usbhidp)
{
    return usbhidSendKey(usbhidp, EVENT_NONE);
}

