/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "dict.h"

#define min(a, b) ((a) > (b) ? (b) : (a))

// 011111000
//  ^    ^
//  from to
#define bitmask(from, to) (0xff & (~((0xff << (8 - from)) | (0xff >> to))))

void bit_split(uint8_t *src, size_t src_len, uint8_t num_bits, uint16_t *dest, size_t dest_len)
{
    unsigned int i, j, bit_i;
    uint16_t acc;
    uint8_t bits_left;

    chDbgAssert(num_bits > 0, "num_bits must be more than 0");

    bit_i = 0;
    i = 0;
    for (j = 0; j < dest_len; ++j)
    {
        bits_left = num_bits;
        acc = 0;
        while (bits_left > 0)
        {
            uint8_t bit_to = min(8, bit_i + bits_left);
            uint8_t bm = bitmask(bit_i, bit_to);
            acc = acc << (bit_to - bit_i);
            acc |= 0xff & ((src[i] & bm) >> (8 - bit_to));
            bits_left -= bit_to - bit_i;
            bit_i = bit_to % 8;
            if (!bit_i)
                i = (i + 1) % src_len; // wrap to start
        }
        dest[j] = acc;
    }
}

static unsigned char base32_enc[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";

void base32(uint8_t *bytes, size_t len, char *output)
{
    chDbgAssert(len % 5 == 0, "base32 does not support padding");

    size_t dest_len = len / 5 * 8;
    uint16_t dest[dest_len];
    bit_split(bytes, len, 5, dest, dest_len);

    for(unsigned int i = 0; i < dest_len; ++i)
        *(output++) = base32_enc[dest[i]];
}

