/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>

#include "hal.h"
#include "usbcfg.h"
#include "usbhid.h"


/*
 * Endpoints to be used for USBD1.
 */
#define USBD1_DATA_REQUEST_EP           1
#define USBD1_DATA_AVAILABLE_EP         1
#define USBD1_INTERRUPT_REQUEST_EP      2
#define USBD1_HID_IN_EP                 3

SerialUSBDriver SDU1;

USBHIDDriver USBHID1;

/*
 * USB Device Descriptor.
 */
static const uint8_t usb_device_descriptor_data[18] = {
    USB_DESC_DEVICE       (0x0111,        /* bcdUSB (1.1).                    */
                           0x00,          /* bDeviceClass (CDC).              */
                           0x00,          /* bDeviceSubClass.                 */
                           0x00,          /* bDeviceProtocol.                 */
                           0x40,          /* bMaxPacketSize.                  */
                           0xFEED,        /* idVendor.                        */
                           0x5788,        /* idProduct.                       */
                           0x0200,        /* bcdDevice.                       */
                           1,             /* iManufacturer.                   */
                           2,             /* iProduct.                        */
                           3,             /* iSerialNumber.                   */
                           1)             /* bNumConfigurations.              */
};

/*
 * Device Descriptor wrapper.
 */
static const USBDescriptor usb_device_descriptor = {
    sizeof usb_device_descriptor_data,
    usb_device_descriptor_data
};

/*
 * HID Report Descriptor.
 */
static const uint8_t hid_report_descriptor_data[] = {
    HID_RI_USAGE_PAGE(8, 0x01),                /* Generic Desktop */
    HID_RI_USAGE(8, 0x06),                     /* Keyboard */
    HID_RI_COLLECTION(8, 0x01),                /* Application */
        HID_RI_USAGE_PAGE(8, 0x07),            /* Key Codes */
        HID_RI_USAGE_MINIMUM(8, 0xE0),         /* Keyboard Left Control */
        HID_RI_USAGE_MAXIMUM(8, 0xE7),         /* Keyboard Right GUI */
        HID_RI_LOGICAL_MINIMUM(8, 0x00),
        HID_RI_LOGICAL_MAXIMUM(8, 0x01),
        HID_RI_REPORT_COUNT(8, 0x08),
        HID_RI_REPORT_SIZE(8, 0x01),
        HID_RI_INPUT(8, HID_IOF_DATA | HID_IOF_VARIABLE | HID_IOF_ABSOLUTE),

        HID_RI_REPORT_COUNT(8, 0x01),
        HID_RI_REPORT_SIZE(8, 0x08),
        HID_RI_INPUT(8, HID_IOF_CONSTANT),     /* Reserved */

        HID_RI_USAGE_PAGE(8, 0x08),            /* LEDs */
        HID_RI_USAGE_MINIMUM(8, 0x01),         /* Num Lock */
        HID_RI_USAGE_MAXIMUM(8, 0x05),         /* Kana */
        HID_RI_REPORT_COUNT(8, 0x05),
        HID_RI_REPORT_SIZE(8, 0x01),
        HID_RI_OUTPUT(8, HID_IOF_DATA | HID_IOF_VARIABLE | HID_IOF_ABSOLUTE | HID_IOF_NON_VOLATILE),
        HID_RI_REPORT_COUNT(8, 0x01),
        HID_RI_REPORT_SIZE(8, 0x03),
        HID_RI_OUTPUT(8, HID_IOF_CONSTANT),    /* Led padding */

        HID_RI_USAGE_PAGE(8, 0x07),            /* Keyboard */
        HID_RI_USAGE_MINIMUM(8, 0x00),         /* Reserved (no event indicated) */
        HID_RI_USAGE_MAXIMUM(8, 0xFF),         /* Keyboard Application */
        HID_RI_LOGICAL_MINIMUM(8, 0x00),
        HID_RI_LOGICAL_MAXIMUM(8, 0xFF),
        HID_RI_REPORT_COUNT(8, 0x06),
        HID_RI_REPORT_SIZE(8, 0x08),
        HID_RI_INPUT(8, HID_IOF_DATA | HID_IOF_ARRAY | HID_IOF_ABSOLUTE),
    HID_RI_END_COLLECTION(0),
};

/*
 * Configuration Descriptor wrapper.
 */
static const USBDescriptor hid_report_descriptor = {
    sizeof hid_report_descriptor_data,
    hid_report_descriptor_data
};

/* Configuration Descriptor tree for a CDC.*/
static const uint8_t usb_configuration_descriptor_data[92] = {
    /* Configuration Descriptor.*/
    USB_DESC_CONFIGURATION(92,            /* wTotalLength.                    */
                           0x03,          /* bNumInterfaces.                  */
                           0x01,          /* bConfigurationValue.             */
                           0,             /* iConfiguration.                  */
                           0xC0,          /* bmAttributes (self powered).     */
                           50),           /* bMaxPower (100mA).               */
    /* Interface Descriptor.*/
    USB_DESC_INTERFACE    (0x00,          /* bInterfaceNumber.                */
                           0x00,          /* bAlternateSetting.               */
                           0x01,          /* bNumEndpoints.                   */
                           0x02,          /* bInterfaceClass (Communications
                                             Interface Class, CDC section
                                             4.2).                            */
                           0x02,          /* bInterfaceSubClass (Abstract
                                             Control Model, CDC section 4.3). */
                           0x01,          /* bInterfaceProtocol (AT commands,
                                             CDC section 4.4).                */
                           0x04),         /* iInterface.                      */
    /* Header Functional Descriptor (CDC section 5.2.3).*/
    USB_DESC_BYTE         (5),            /* bLength.                         */
    USB_DESC_BYTE         (0x24),         /* bDescriptorType (CS_INTERFACE).  */
    USB_DESC_BYTE         (0x00),         /* bDescriptorSubtype (Header
                                             Functional Descriptor.           */
    USB_DESC_BCD          (0x0110),       /* bcdCDC.                          */
    /* Call Management Functional Descriptor. */
    USB_DESC_BYTE         (5),            /* bFunctionLength.                 */
    USB_DESC_BYTE         (0x24),         /* bDescriptorType (CS_INTERFACE).  */
    USB_DESC_BYTE         (0x01),         /* bDescriptorSubtype (Call Management
                                             Functional Descriptor).          */
    USB_DESC_BYTE         (0x00),         /* bmCapabilities (D0+D1).          */
    USB_DESC_BYTE         (0x01),         /* bDataInterface.                  */
    /* ACM Functional Descriptor.*/
    USB_DESC_BYTE         (4),            /* bFunctionLength.                 */
    USB_DESC_BYTE         (0x24),         /* bDescriptorType (CS_INTERFACE).  */
    USB_DESC_BYTE         (0x02),         /* bDescriptorSubtype (Abstract
                                             Control Management Descriptor).  */
    USB_DESC_BYTE         (0x02),         /* bmCapabilities.                  */
    /* Union Functional Descriptor.*/
    USB_DESC_BYTE         (5),            /* bFunctionLength.                 */
    USB_DESC_BYTE         (0x24),         /* bDescriptorType (CS_INTERFACE).  */
    USB_DESC_BYTE         (0x06),         /* bDescriptorSubtype (Union
                                             Functional Descriptor).          */
    USB_DESC_BYTE         (0x00),         /* bMasterInterface (Communication
                                             Class Interface).                */
    USB_DESC_BYTE         (0x01),         /* bSlaveInterface0 (Data Class
                                             Interface).                      */
    /* Endpoint 2 Descriptor.*/
    USB_DESC_ENDPOINT     (USBD1_INTERRUPT_REQUEST_EP|0x80,
                                          /* bEndpointAddress.                */
                           0x03,          /* bmAttributes (Interrupt).        */
                           0x0008,        /* wMaxPacketSize.                  */
                           0xFF),         /* bInterval.                       */
    /* Interface Descriptor.*/
    USB_DESC_INTERFACE    (0x01,          /* bInterfaceNumber.                */
                           0x00,          /* bAlternateSetting.               */
                           0x02,          /* bNumEndpoints.                   */
                           0x0A,          /* bInterfaceClass (Data Class
                                             Interface, CDC section 4.5).     */
                           0x00,          /* bInterfaceSubClass (CDC section
                                             4.6).                            */
                           0x00,          /* bInterfaceProtocol (CDC section
                                             4.7).                            */
                           0x00),         /* iInterface.                      */
    /* Endpoint 3 Descriptor.*/
    USB_DESC_ENDPOINT     (USBD1_DATA_AVAILABLE_EP,
                                          /* bEndpointAddress.                */
                           0x02,          /* bmAttributes (Bulk).             */
                           0x0040,        /* wMaxPacketSize.                  */
                           0x00),         /* bInterval.                       */
    /* Endpoint 1 Descriptor.*/
    USB_DESC_ENDPOINT     (USBD1_DATA_REQUEST_EP|0x80,
                                          /* bEndpointAddress.                */
                           0x02,          /* bmAttributes (Bulk).             */
                           0x0040,        /* wMaxPacketSize.                  */
                           0x00),         /* bInterval.                       */
    /* Interface Descriptor.*/
    USB_DESC_INTERFACE    (0x02,          /* bInterfaceNumber.                */
                           0x00,          /* bAlternateSetting.               */
                           0x01,          /* bNumEndpoints.                   */
                           0x03,          /* bInterfaceClass (Human
                                             Interface Device).               */
                           0x00,          /* bInterfaceSubClass (None).       */
                           0x00,          /* bInterfaceProtocol (None).       */
                           0x05),         /* iInterface.                      */
    /* HID Descriptor.*/
    USB_DESC_BYTE         (9),            /* bLength.                         */
    USB_DESC_BYTE         (0x21),         /* bDescriptorType (HID Class).     */
    USB_DESC_BCD          (0x0111),       /* bcdHID (HID Version 1.11).       */
    USB_DESC_BYTE         (0x00),         /* bCountryCode.                    */
    USB_DESC_BYTE         (0x01),         /* bNumDescriptors.                 */
    USB_DESC_BYTE         (0x22),         /* bDescriptorType (Report).        */
    USB_DESC_WORD         (sizeof hid_report_descriptor_data),
                                          /* wDescriptorLength                */
    /* Endpoint 4 Descriptor.*/
    USB_DESC_ENDPOINT     (USBD1_HID_IN_EP|0x80,
                                          /* bEndpointAddress.                */
                           0x03,          /* bmAttributes (Interrupt).        */
                           USB_HID_REPORT_SIZE,
                                          /* wMaxPacketSize.                  */
                           0x0A)          /* bInterval.                       */
};

/*
 * Configuration Descriptor wrapper.
 */
static const USBDescriptor usb_configuration_descriptor = {
    sizeof usb_configuration_descriptor_data,
    usb_configuration_descriptor_data
};

/*
 * U.S. English language identifier.
 */
static const uint8_t usb_string0[] = {
    USB_DESC_BYTE(4),                     /* bLength.                         */
    USB_DESC_BYTE(USB_DESCRIPTOR_STRING), /* bDescriptorType.                 */
    USB_DESC_WORD(0x0409)                 /* wLANGID (U.S. English).          */
};

/*
 * Vendor string.
 */
static const uint8_t usb_string1[] = {
    USB_DESC_BYTE(40),                    /* bLength.                         */
    USB_DESC_BYTE(USB_DESCRIPTOR_STRING), /* bDescriptorType.                 */
    'Y', 0, '.', 0, 'O', 0, '.', 0, 'B', 0, '.', 0, 'A', 0, '.', 0,
    ' ', 0, 'I', 0, 'n', 0, 'd', 0, 'u', 0, 's', 0, 't', 0, 'r', 0,
    'i', 0, 'e', 0, 's', 0
};

/*
 * Dev Description string.
 */
static const uint8_t usb_string2[] = {
    USB_DESC_BYTE(46),                    /* bLength.                         */
    USB_DESC_BYTE(USB_DESCRIPTOR_STRING), /* bDescriptorType.                 */
    'P', 0, 'a', 0, 's', 0, 's', 0, 'p', 0, 'o', 0, 'r', 0, '2', 0,
    ' ', 0, 'P', 0, 'a', 0, 's', 0, 's', 0, 'w', 0, 'o', 0, 'r', 0,
    'd', 0, ' ', 0, 'S', 0, 'a', 0, 'f', 0, 'e', 0
};

/*
 * Serial Number string.
 */
static const uint8_t usb_string3[] = {
    USB_DESC_BYTE(8),                     /* bLength.                         */
    USB_DESC_BYTE(USB_DESCRIPTOR_STRING), /* bDescriptorType.                 */
    '0' + CH_KERNEL_MAJOR, 0,
    '0' + CH_KERNEL_MINOR, 0,
    '0' + CH_KERNEL_PATCH, 0
};

/*
 * VCOM Description string.
 */
static const uint8_t usb_string4[] = {
    USB_DESC_BYTE(28),                    /* bLength.                         */
    USB_DESC_BYTE(USB_DESCRIPTOR_STRING), /* bDescriptorType.                 */
    'P', 0, 'a', 0, 's', 0, 's', 0, 'p', 0, 'o', 0, 'r', 0, '2', 0,
    ' ', 0, 'V', 0, 'C', 0, 'O', 0, 'M', 0
};

/*
 * HID Description string.
 */
static const uint8_t usb_string5[] = {
    USB_DESC_BYTE(26),                    /* bLength.                         */
    USB_DESC_BYTE(USB_DESCRIPTOR_STRING), /* bDescriptorType.                 */
    'P', 0, 'a', 0, 's', 0, 's', 0, 'p', 0, 'o', 0, 'r', 0, '2', 0,
    ' ', 0, 'H', 0, 'I', 0, 'D', 0
};

/*
 * Strings wrappers array.
 */
static const USBDescriptor usb_strings[] = {
    {sizeof usb_string0, usb_string0},
    {sizeof usb_string1, usb_string1},
    {sizeof usb_string2, usb_string2},
    {sizeof usb_string3, usb_string3},
    {sizeof usb_string4, usb_string4},
    {sizeof usb_string5, usb_string5},
};

/*
 * Handles the GET_DESCRIPTOR callback. All required descriptors must be
 * handled here.
 */
static const USBDescriptor *get_descriptor(USBDriver *usbp,
        uint8_t dtype,
        uint8_t dindex,
        uint16_t lang)
{
    (void)usbp;
    (void)lang;
    switch (dtype) {
        case USB_DESCRIPTOR_DEVICE:
            return &usb_device_descriptor;
        case USB_DESCRIPTOR_CONFIGURATION:
            return &usb_configuration_descriptor;
        case USB_DESCRIPTOR_STRING:
            if (dindex < 6)
                return &usb_strings[dindex];
            break;

            // HID specific descriptors
        case USB_DESCRIPTOR_HID_REPORT:    // HID Report Descriptor
            return &hid_report_descriptor;
    }
    return NULL;
}

/**
 * @brief   IN EP1 state.
 */
static USBInEndpointState ep1instate;

/**
 * @brief   OUT EP1 state.
 */
static USBOutEndpointState ep1outstate;

/**
 * @brief   EP1 initialization structure (both IN and OUT).
 */
static const USBEndpointConfig ep1config = {
    USB_EP_MODE_TYPE_BULK,
    NULL,
    sduDataTransmitted,
    sduDataReceived,
    0x0040,
    0x0040,
    &ep1instate,
    &ep1outstate,
    2,
    NULL
};

/**
 * @brief   IN EP2 state.
 */
static USBInEndpointState ep2instate;

/**
 * @brief   EP2 initialization structure (IN only).
 */
static const USBEndpointConfig ep2config = {
    USB_EP_MODE_TYPE_INTR,
    NULL,
    sduInterruptTransmitted,
    NULL,
    0x0010,
    0x0000,
    &ep2instate,
    NULL,
    1,
    NULL
};

/**
 * @brief   IN EP3 state.
 */
static USBInEndpointState ep3instate;

/**
 * @brief   EP3 initialization structure (both IN and OUT).
 */
static const USBEndpointConfig ep3config = {
    USB_EP_MODE_TYPE_INTR,
    NULL,
    usbhidDataTransmitted,
    NULL,
    0x0008,
    0x0000,
    &ep3instate,
    NULL,
    1,
    NULL
};

/*
 * Handles the USB driver global events.
 */
static void usb_event(USBDriver *usbp, usbevent_t event)
{
    extern SerialUSBDriver SDU1;

    switch (event)
    {
        case USB_EVENT_ADDRESS:
            return;
        case USB_EVENT_CONFIGURED:
            chSysLockFromISR();

            /* Enables the endpoints specified into the configuration.
               Note, this callback is invoked from an ISR so I-Class functions
               must be used.*/
            usbInitEndpointI(usbp, USBD1_DATA_REQUEST_EP, &ep1config);
            usbInitEndpointI(usbp, USBD1_INTERRUPT_REQUEST_EP, &ep2config);

            /* Resetting the state of the CDC subsystem.*/
            sduConfigureHookI(&SDU1);

            usbInitEndpointI(usbp, USBD1_HID_IN_EP, &ep3config);

            usbhidConfigureHookI(&USBHID1);

            chSysUnlockFromISR();
            return;
        case USB_EVENT_RESET:
            /* Falls into.*/
        case USB_EVENT_UNCONFIGURED:
            /* Falls into.*/
        case USB_EVENT_SUSPEND:
            chSysLockFromISR();

            /* Disconnection event on suspend.*/
            sduSuspendHookI(&SDU1);
            usbhidSuspendHookI(&USBHID1);

            chSysUnlockFromISR();
            return;
        case USB_EVENT_WAKEUP:
            chSysLockFromISR();

            /* Connection event on wakeup.*/
            sduWakeupHookI(&SDU1);
            usbhidWakeupHookI(&USBHID1);

            chSysUnlockFromISR();
            return;
        case USB_EVENT_STALLED:
            return;
    }
    return;
}

static bool
usb_request_hook (USBDriver * usbp)
{
    if (usbhidRequestsHook(usbp))
        return true;
    return sduRequestsHook(usbp);
}

/*
 * Handles the USB driver global events.
 */
static void sof_handler(USBDriver *usbp)
{
    (void)usbp;

    osalSysLockFromISR();
    sduSOFHookI(&SDU1);
    osalSysUnlockFromISR();
}

/*
 * USB driver configuration.
 */
const USBConfig usbcfg = {
    usb_event,
    get_descriptor,
    usb_request_hook,
    sof_handler
};

/*
 * Serial over USB driver configuration.
 */
const SerialUSBConfig serusbcfg = {
    &USBD1,
    USBD1_DATA_REQUEST_EP,
    USBD1_DATA_AVAILABLE_EP,
    USBD1_INTERRUPT_REQUEST_EP
};

/*
 * USB HID driver configuration.
 */
const USBHIDConfig usbhidcfg = {
    &USBD1,
    USBD1_HID_IN_EP
};

