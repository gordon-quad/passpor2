/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <stdio.h>

#include "hal.h"
#include "btn.h"
#include "led.h"
#include "typer.h"
#include "pwd.h"
#include "ctrl.h"

#define EVENT_SHORT_PRESS 0
#define EVENT_LONG_PRESS  1
#define EVENT_ARM         2

static THD_WORKING_AREA(ctrl_wa, 1024);

static event_listener_t btn0_lst, btn1_lst, arm_lst;

static EVENTSOURCE_DECL(arm_evt);

static char arm_buf[ARM_BUF_SIZE];
static size_t arm_len=0;

static MUTEX_DECL(arm_mtx);

void arm_next_static_pwd(uint16_t c)
{
    pwd_static(c, arm_buf);
    arm_len = PWD_LEN;
}

void fire(void)
{
    typer_type(arm_buf, arm_len);
}

THD_FUNCTION(ctrl_thread, p)
{
    (void)p;
    bool armed = false;
    bool static_armed = false;
    uint16_t static_counter = 0;
    uint8_t click_counter = 0;
    sysinterval_t timeout = TIME_INFINITE;

    chRegSetThreadName("ctrl");
    chEvtRegister(&btn_short_press_evt, &btn0_lst, EVENT_SHORT_PRESS);
    chEvtRegister(&btn_long_press_evt, &btn1_lst, EVENT_LONG_PRESS);
    chEvtRegister(&arm_evt, &arm_lst, EVENT_ARM);

    while (1)
    {
        eventmask_t evts;
        evts = chEvtWaitOneTimeout(ALL_EVENTS, timeout);
        if (!evts)
        {
            timeout = TIME_INFINITE;
            click_counter = 0;
        }
        if (evts & EVENT_MASK(EVENT_ARM))
        {
            if (!armed)
            {
                led_set(LED_RED, LED_MODE_ON);
                armed = true;
                static_armed = false;
                static_counter = 0;
                timeout = TIME_INFINITE;
                click_counter = 0;
            }
        }
        if (evts & EVENT_MASK(EVENT_SHORT_PRESS))
        {
            if ((static_armed && armed) || !armed)
            {
                chMtxLock(&arm_mtx);
                static_armed = true;
                if (!armed)
                    led_set(LED_RED, LED_MODE_ON);
                armed = true;
                led_set(LED_GREEN, LED_MODE_SHORT_FLASH);
                arm_next_static_pwd(static_counter++);
                timeout = TIME_INFINITE;
                click_counter = 0;
                chMtxUnlock(&arm_mtx);
            }
            else if (armed)
            {
                click_counter++;
                timeout = CLICK_SERIES_TIMEOUT;
                if (click_counter >= CLICKS_TO_DISARM)
                {
                    chMtxLock(&arm_mtx);
                    armed = false;
                    led_set(LED_RED, LED_MODE_OFF);
                    static_armed = false;
                    static_counter = 0;
                    click_counter = 0;
                    chMtxUnlock(&arm_mtx);
                }
            }
        }
        if (evts & EVENT_MASK(EVENT_LONG_PRESS))
        {
            if (armed)
            {
                chMtxLock(&arm_mtx);
                fire();
                timeout = TIME_INFINITE;
                click_counter = 0;
                armed = false;
                static_armed = false;
                static_counter = 0;
                led_set(LED_RED, LED_MODE_OFF);
                chMtxUnlock(&arm_mtx);
            }
        }
    }
}

void ctrl_init(void)
{
    // Nothing to be done
}

void ctrl_start(void)
{
    chThdCreateStatic(ctrl_wa, sizeof(ctrl_wa),
            NORMALPRIO, ctrl_thread, NULL);
}

void ctrl_arm(char *str, size_t len)
{
    size_t n;
    chDbgAssert(len <= ARM_BUF_SIZE, "arm string is too long");

    n = len <= ARM_BUF_SIZE ? len : ARM_BUF_SIZE;
    chMtxLock(&arm_mtx);
    memcpy(arm_buf, str, n);
    arm_len = n;
    chEvtBroadcast(&arm_evt);
    chMtxUnlock(&arm_mtx);
}
