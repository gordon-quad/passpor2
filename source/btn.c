/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "hal.h"
#include "btn.h"

EVENTSOURCE_DECL(btn_short_press_evt);
EVENTSOURCE_DECL(btn_long_press_evt);

static THD_WORKING_AREA(btn_wa, 128);

THD_FUNCTION(btn_thread, p)
{
    (void)p;
    bool pressed = false;
    bool long_press_wait = false;
    bool long_press_sent = false;
    msg_t res;
    systime_t long_press_time = 0;

    chRegSetThreadName("btn");
    palEnableLineEvent(LINE_BTN1, PAL_EVENT_MODE_BOTH_EDGES);

    while (1)
    {
        bool cur;
        res = palWaitLineTimeout(LINE_BTN1, DEBOUNCE_FILTER_TIME);

        if (res == MSG_TIMEOUT)
        {
            // Button stablilized, process
            cur = palReadLine(LINE_BTN1) == BTN1_PRESSED_STATE;

            if (cur != pressed)
            {
                pressed = cur;
                if (pressed)
                {
                    long_press_time = chVTGetSystemTime();
                    long_press_wait = true;
                    long_press_sent = false;
                }
                else if (!long_press_sent)
                {
                    // send short press
                    chEvtBroadcast(&btn_short_press_evt);
                    long_press_wait = false;
                    long_press_sent = false;
                }
                else
                {
                    // we already sent long press event
                    long_press_wait = false;
                    long_press_sent = false;
                }
            }
            if (long_press_wait && (chVTTimeElapsedSinceX(long_press_time) > LONG_PRESS_DURATION))
            {
                chEvtBroadcast(&btn_long_press_evt);
                long_press_sent = true;
                long_press_wait = false;
            }
        }
    }
}

void btn_init(void)
{
    // Nothing to be done
}

void btn_start(void)
{
    chThdCreateStatic(btn_wa, sizeof(btn_wa),
            NORMALPRIO + 10, btn_thread, NULL);
}
