/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "typer.h"
#include "usbhid.h"
#include "usbcfg.h"

static THD_WORKING_AREA(typer_wa, 128);

static uint8_t typer_pipe_buf[TYPER_BUF_LEN];
static PIPE_DECL(typer_pipe, typer_pipe_buf, TYPER_BUF_LEN);

                             //         ' '            '!'                '"'            '#'            '$'            '%'            '&'            '''            '('            ')'                    '*'                    '+'                    ','                     '-'                   '.'                '/'
const uint16_t usb_codes[] = {    KEY_SPACE, _SHIFT(KEY_1), _SHIFT(KEY_QUOTE), _SHIFT(KEY_3), _SHIFT(KEY_4), _SHIFT(KEY_5), _SHIFT(KEY_7),     KEY_QUOTE, _SHIFT(KEY_9), _SHIFT(KEY_0),         _SHIFT(KEY_8),     _SHIFT(KEY_EQUAL),             KEY_COMMA,              KEY_MINUS,           KEY_PERIOD,         KEY_SLASH,
                             //         '0'            '1'                '2'            '3'            '4'            '5'            '6'            '7'            '8'            '9'                    ':'                    ';'                    '<'                     '='                   '>'                '?'
                                      KEY_0,         KEY_1,           KEY_2,           KEY_3,         KEY_4,         KEY_5,         KEY_6,         KEY_7,         KEY_8,         KEY_9, _SHIFT(KEY_SEMICOLON),         KEY_SEMICOLON,     _SHIFT(KEY_COMMA),              KEY_EQUAL,   _SHIFT(KEY_PERIOD), _SHIFT(KEY_SLASH),
                             //         '@'            'A'                'B'            'C'            'D'            'E'            'F'            'G'            'H'            'I'                    'J'                    'K'                    'L'                     'M'                   'N'                'O'
                              _SHIFT(KEY_2), _SHIFT(KEY_A),     _SHIFT(KEY_B), _SHIFT(KEY_C), _SHIFT(KEY_D), _SHIFT(KEY_E), _SHIFT(KEY_F), _SHIFT(KEY_G), _SHIFT(KEY_H), _SHIFT(KEY_I),         _SHIFT(KEY_J),         _SHIFT(KEY_K),         _SHIFT(KEY_L),          _SHIFT(KEY_M),        _SHIFT(KEY_N),     _SHIFT(KEY_O),
                             //         'P'            'Q'                'R'            'S'            'T'            'U'            'V'            'W'            'X'            'Y'                    'Z'                    '['                    '\'                     ']'                   '^'                '_'
                              _SHIFT(KEY_P), _SHIFT(KEY_Q),     _SHIFT(KEY_R), _SHIFT(KEY_S), _SHIFT(KEY_T), _SHIFT(KEY_U), _SHIFT(KEY_V), _SHIFT(KEY_W), _SHIFT(KEY_X), _SHIFT(KEY_Y),         _SHIFT(KEY_Z),         KEY_LEFTBRACE,         KEY_BACKSLASH,         KEY_RIGHTBRACE,        _SHIFT(KEY_6), _SHIFT(KEY_MINUS),
                             //         '`'            'a'                'b'            'c'            'd'            'e'            'f'            'g'            'h'            'i'                    'j'                    'k'                    'l'                     'm'                   'n'                'o'
                               KEY_BACKTICK,         KEY_A,             KEY_B,         KEY_C,         KEY_D,         KEY_E,         KEY_F,         KEY_G,         KEY_H,         KEY_I,                 KEY_J,                 KEY_K,                 KEY_L,                  KEY_M,                KEY_N,             KEY_O,
                             //         'p'            'q'                'r'            's'            't'            'u'            'v'            'w'            'x'            'y'                    'z'                    '{'                    '|'                     '}'                   '~'
                                      KEY_P,         KEY_Q,             KEY_R,         KEY_S,         KEY_T,         KEY_U,         KEY_V,         KEY_W,         KEY_X,         KEY_Y,                 KEY_Z, _SHIFT(KEY_LEFTBRACE), _SHIFT(KEY_BACKSLASH), _SHIFT(KEY_RIGHTBRACE), _SHIFT(KEY_BACKTICK)};

void usb_type_char(char c)
{
    if ((c >= ' ') && (c < 127))
    {
        uint16_t code = usb_codes[c - ' '];
        usbhidSendKey(&USBHID1, code);
        usbhidReleaseKeys(&USBHID1);
    }
}

THD_FUNCTION(typer_thread, p)
{
    (void)p;
    char c;
    size_t n;

    chRegSetThreadName("typer");
    while (1)
    {
        if((n = chPipeReadTimeout(&typer_pipe, (uint8_t *)&c, 1, TIME_INFINITE)) > 0)
        {
            usb_type_char(c);
            chThdSleepMilliseconds(TYPER_DELAY_MS);
        }
    }
}

void typer_init(void)
{
    // Nothing to be done
}

void typer_start(void)
{
    chThdCreateStatic(typer_wa, sizeof(typer_wa),
            NORMALPRIO, typer_thread, NULL);
}

void typer_type(char *str, size_t len)
{
    size_t n = 0;
    while (len > 0)
    {
        n = chPipeWriteTimeout(&typer_pipe, (uint8_t *)str, len, TIME_INFINITE);
        str += n;
        len -= n;
    }
}
