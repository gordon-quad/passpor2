/*
   passpor2 - Copyright © 2019 Gordon Quad

   This file is part of passpor2.

   passpor2 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   passpor2 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with passpor2.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _PWD_H_
#define _PWD_H_

#include "dict.h"

#define PWD_WORD_LEN    8
#define PWD_SEP         '.'
#define PWD_APPEND      "123!@#"
#define PWD_APPEND_LEN  6
#define PWD_LEN         ((WORD_LEN*PWD_WORD_LEN) + (PWD_WORD_LEN-1) + PWD_APPEND_LEN)
#define MAX_URL_LEN     80

#define OTP_PERIOD      30
#define OTP80_KEY_LEN   10
#define OTP160_KEY_LEN  20

#define OTP_CODE6_LEN   7

void pwd_str(uint8_t *data, size_t data_len, char *dest);
void pwd_static(uint16_t c, char *out);
void pwd_url(char *url, uint16_t c, char *out);
void pwd_otp_key(uint16_t c, size_t key_len, char *out);
void pwd_otp_code(uint16_t c, size_t key_len, uint64_t data, char *out);

#endif//_PWD_H_

